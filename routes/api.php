<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', 'AuthController@login');
Route::post('custom_register', 'AuthController@custom_register');
Route::post('user/cek_daftar', 'AuthController@cekDaftar');

// to do auth jwt / api
Route::group(['middleware' => 'api'], function () {
  // Route::get('test', 'DummyController@index');
  // Route::post('test', 'DummyController@index');
  // Route::get('test/{type}', 'DummyController@index');
  // Route::post('test/{type}', 'DummyController@index');

  Route::group([
    'prefix' => "public",
    'namespace' => 'API\Publik'
  ], function () {
    Route::prefix('transact')->group(
      function () {
        Route::prefix('kepeng')->group(
          function () {
            // Route::post('topup', 'DummyController@index');
            Route::post('topup/callback', 'TransactionController@topupCallback');
            Route::post('payout', 'TransactionController@payout');
            Route::post('payout/callback', 'TransactionController@payoutCallback');
            Route::post('qrcode/pay', 'TransactionController@qrcodePay');
            Route::post('qrcode/callback', 'TransactionController@qrcodeCallback');
            Route::post('fixedva/created/callback', 'TransactionController@fixedvaCreatedCallback');
            Route::post('fixedva/paid/callback', 'TransactionController@fixedvaPaidCallback');
        });
      }
    );
  });
});

/*
  admin/qrcode/create
  admin/qrcode/pay
  admin/qrcode/callback

  get
  admin/qrcode/
  admin/qrcode/regenerate
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('logout', 'AuthController@logout');


    Route::post('user/update_photo', 'AuthController@updatePhoto');
    Route::post('user/update_profil', 'AuthController@updateProfilData');
    Route::post('user/update_pass', 'AuthController@updatePass');
     
      
    Route::get('user', 'AuthController@user');
    Route::get('detail', 'UserController@UserDetails');
    Route::post('check_pin', 'UserController@checkPin');
    Route::post('kirim_uang', 'UserController@KirimUang');
    Route::post('kirim_poin', 'UserController@KirimPoin');


    Route::post('top_up', 'TopUpController@store');

    Route::get('sliders', 'SliderController@sliderAPI');
    Route::get('slider2s', 'SliderBeritaController@sliderAPI');

    //history user api 
    Route::get('history/topup', 'UserHistoryController@historyTopUp');
    Route::get('history/topup/{id}', 'UserHistoryController@topupDetail');
    Route::get('history/transfer', 'UserHistoryController@historyTransfer');
    Route::get('history/point', 'UserHistoryController@historyPenarikanPoint');
    Route::get('history/terima_uang', 'UserHistoryController@historyMintaUang');

    Route::post('pencairan/kirim', 'PencairanController@pencarianRequest');
    Route::get('pencairan/history', 'PencairanController@apiHistory')->name('pencairan.history');

    Route::post('pin', 'UserController@pinCreate');
    Route::post('pin/check', 'UserController@pinCheck');
});
