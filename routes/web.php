<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Route::get('/', function () {
//     return view('login');
// });

Route::get('clear', function() {
   Artisan::call('config:cache');
   return 'Clear Cache Config Succesfull!';
});


Route::get('/', 'Auth\\LoginController@index')->name('admin.login');
Route::get('admin/tentang/panduan/view', 'TentangController@viewPanduan');
Route::get('admin/tentang/syarat/view', 'TentangController@viewSyarat');
Route::get('admin/tentang/kebijakan/view', 'TentangController@viewKebijakan');
Route::get('admin/tentang/bantuan/view', 'TentangController@viewBantuan');
Auth::routes();






Route::group(['middleware' => 'auth',  'prefix' => 'admin', 'as' => 'admin.'], function () {


    Route::resource('category', 'CategoryController');
    Route::get('/home', 'HomeController@index')->name('admin.home');

    Route::get('user/pengguna', 'UserController@pengguna');
  
  
    Route::get('user/admin', 'UserController@admin');
    Route::get('user/admin/create', 'UserController@adminCreate');
    Route::POST('user/admin/store', 'UserController@adminStore');
    Route::get('user/admin/edit/{id}', 'UserController@adminEdit');
       Route::get('user/admin/edit_list/{id}', 'UserController@adminEditList');
    
    Route::post('user/admin/update/{id}', 'UserController@adminUpdate');
    
       Route::post('user/admin/update/list/{id}', 'UserController@adminUpdateList');
    
    
    Route::post('user/pengguna/status/{id}', 'UserController@statusUSer');
    Route::post('user/pengguna/resetpass/{id}', 'UserController@resetPassUser');
    Route::post('user/pengguna/resetpin/{id}', 'UserController@resetPinUser');

    Route::resource('user', 'UserController');
    Route::resource('tag', 'TagController');
    Route::resource('post', 'PostController');

    Route::resource('roles', 'Admin\RolesController');
    Route::resource('permissions', 'Admin\PermissionsController');

    //baru
    Route::get('slider/api', 'SliderController@sliderAPI');
    Route::resource('slider', 'SliderController');
      Route::resource('slider_berita', 'SliderBeritaController');

    Route::post('topuphistory/masuk/verifikasi/{id}', 'TopUpHistoryController@verifikasiTopupMasuk');
    Route::post('topuphistory/masuk/reject/{id}', 'TopUpHistoryController@rejectTopupMasuk');

    Route::get('topuphistory/masuk', 'TopUpHistoryController@topupMasuk')->name('admin.topuphistory.masuk');
    Route::get('topuphistory/manual', 'TopUpHistoryController@topupManual');
    Route::get('topuphistory/admin_history', 'TopUpHistoryController@adminHistory');

    Route::get('topuphistory/point', 'TopUpHistoryController@tambahPointIndex');
    Route::post('topuphistory/point/add', 'TopUpHistoryController@tambahPoint');
    Route::post('topuphistory/admintopup', 'TopUpHistoryController@tambahkepeng');
    Route::resource('topuphistory', 'TopUpHistoryController');

    Route::get('kirim/history', 'KirimHistoryController@index');

    //pengaturan
    Route::get('pengaturan/fee', 'PengaturanController@fee');
    Route::post('pengaturan/fee/update', 'PengaturanController@updateFee');

    //tentang
    Route::get('tentang/panduan', 'TentangController@panduan');
    Route::get('tentang/syarat', 'TentangController@syarat');
    Route::get('tentang/kebijakan', 'TentangController@kebijakan');
    Route::get('tentang/bantuan', 'TentangController@bantuan');

    Route::get('tentang/', 'TentangController@index');
    Route::post('tentang/panduan/simpan', 'TentangController@storePanduan');
    Route::post('tentang/syarat/simpan', 'TentangController@storeSyarat');
    Route::post('tentang/kebijakan/simpan', 'TentangController@storeKebijakan');
    Route::post('tentang/bantuan/simpan', 'TentangController@storeBantuan');

    //pencairan
    Route::get('pencairan/history', 'PencairanController@history')->name('pencairan.history');
    Route::post('pencairan/verifikasi/{id}', 'PencairanController@verifikasi')->name('pencairan.verifikasi');
    Route::post('pencairan/reject/{id}', 'PencairanController@reject')->name('pencairan.reject');
    Route::resource('pencairan', 'PencairanController');
});


Route::get('autologin', function(){
    Auth::loginUsingId(1);

    return redirect()->to('/');
})->middleware('web');