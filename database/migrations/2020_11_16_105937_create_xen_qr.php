<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateXenQr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xen_qr', function (Blueprint $table) {
            $table->id();
            $table->string('external_id');
            $table->integer('amount')->nullable();
            $table->text('qr_string')->default('-');
            $table->string('callback_url')->nullable();
            $table->string('type',100)->nullable();
            $table->string('status',100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xen_qr');
    }
}
