<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersFixedVa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_fixed_va', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("user_id")->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on("users")
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->boolean("is_closed")->nullable();
            $table->string("status")->nullable();
            $table->string("currency")->nullable();
            $table->string("owner_id")->nullable();
            $table->string("external_id")->nullable();
            $table->string("bank_code")->nullable();
            $table->string("merchant_code")->nullable();
            $table->string("name")->nullable();
            $table->string("account_number");
            $table->string("is_single_use")->nullable();
            $table->dateTime("expiration_date")->nullable();
            $table->string("xen_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_fixed_va');
    }
}
