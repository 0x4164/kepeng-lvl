<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_url', 100)->nullable();
            $table->bigInteger('id_transaction')->unsigned()->nullable();
            // $table->foreign("id_transaction")->references('id')->on('transactions')
            // ->onUpdate('cascade')
            // ->onDelete("set null");

            $table->string('external_id');
            $table->string('user_id');
            $table->string('is_high')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('status',100)->nullable();
            $table->string('merchant_name')->nullable();
            $table->integer('amount')->nullable();
            $table->integer('paid_amount')->nullable();
            $table->string('bank_code')->nullable();
            $table->dateTimeTz('paid_at')->nullable();
            $table->string('payer_email')->nullable();
            $table->string('description')->nullable();
            $table->integer('adjusted_received_amount')->nullable();
            $table->integer('fees_paid_amount')->nullable();
            $table->dateTime('expiry_date')->nullable();

            $table->dateTime('updated')->nullable(); // by payment gateway
            $table->dateTime('created')->nullable(); // by payment gateway
            $table->string('currency')->nullable();
            $table->string('payment_channel')->nullable();
            $table->string('payment_destination')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
