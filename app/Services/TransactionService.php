<?php

namespace App\Services;

use App\Services\Abstracts\AbstractService;
use App\Services\Transaction\NullTransact;
use App\Services\Transaction\CheckoutTransact;
use App\Services\Transaction\PayCallbackTransact;
use App\Services\Transaction\PayoutCallbackTransact;
use App\Services\Transaction\PayoutTransact;
use App\Services\Transaction\{
    QRCodeCreateTransact, QRCodeStatusTransact, QRCodePayTransact, QRCodeCallbackTransact
};
use App\Services\Transaction\{
    CreateFixedVA
};
use App\Helpers\Constants;
// use App\Helpers\Dummy;
// use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;

class TransactionService extends AbstractService
{
    protected $type;
    protected $user;
    protected $model;

    public function __construct()
    {
        
    }

    public function setType($type)
    {
        $this->type = $type;
        
        return $this;
    }

    // get trans by userid & token
    public function getTransaction($token)
    {
        $trans = Transaction::where("token", $token)->firstOrFail();
        return $trans;
    }

    public function error($process, $e)
    {
        $ret = new stdClass();
        $process->setPublicResult($e->getMessage());
        $ret->result = $process->proccess();
        $ret->code = 500;
        return $ret;
    }

    public function proccess($req = null)
    {
        $p = new NullTransact();

        switch ($this->type) {
            case Constants::TRANS_CHECKOUT:
                $p = new CheckoutTransact();
                $p->setData($this->getData());

                break;
            // case Constants::TRANS_CANCEL_BY_PEOPLE:
            //     $p = new OrderCancelBySystem();
            //     break;
            case Constants::TRANS_PAY_CALLBACK:
                //call from payment gateway
                $p = new PayCallbackTransact();
                break;
            case Constants::TRANS_PAYOUT:
                $p = new PayoutTransact();
                $p->setData($this->getData());
                break;
            case Constants::TRANS_PAYOUT_CALLBACK:
                $p = new PayoutCallbackTransact();
                break;
            case Constants::TRANS_QRCODE_CREATE:
                $p = new QRCodeCreateTransact();
                $p->setData($this->getData());
                break;
            case Constants::TRANS_QRCODE_STATUS:
                $p = new QRCodeStatusTransact();
                $p->setData($this->getData());
                break;
            case Constants::TRANS_QRCODE_PAY:
                $p = new QRCodePayTransact();
                $p->setData($this->getData());
                break;
            case Constants::TRANS_QRCODE_CALLBACK:
                $p = new QRCodeCallbackTransact();
                break;
            case Constants::TRANS_FINISHED:
                $p = new FinishTransact();
                $trans = $this->getTransaction($req->token);
                $p->setTransaction($trans);
                break;
            case Constants::TRANS_CREATE_FVA:
                $p = new CreateFixedVA();
                $p->setData($this->getData());
                break;
            default:
                echo Constants::TRANS_UNKNOWN . " " . Constants::TRANS_UNKNOWN_MSG;
                $ret = Constants::TRANS_UNKNOWN;
                // throw new TransactionUknownException();
                break;
        }

        $ret = new stdClass();
        $ret->result = $p->proccess();
        $ret->message = $p->getMessage();
        $ret->code = $p->getCode();

        return $ret;
    }
}
