<?php

namespace App\Services\Transaction;

use App\Exceptions\CartInvalid;
use App\Models\User;
use App\Services\Abstracts\TransactionProcess;
// use App\Services\DeliveryService;
use App\Services\Transaction\InvoiceTransact;
use App\Helpers\Utils;
// use App\Helpers\Dummy;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
// use App\Events\CheckoutOccured;

class CheckoutTransact extends TransactionProcess
{
    public $voucher;
    public $delivSvc;

    public function __construct()
    {
        parent::__construct();
        // echo "init";
        $this->result = false;
    }

    protected function validate()
    {
        /*
        validate
        - user
        - cart
         */
        return true;
    }

    protected function verify()
    {
        return true;
    }

    protected function validateUser($user){
        if($user){
            // if(empty($user->email)){
            //     $this->err("Email Kosong");
            // }
        }else{
            $this->err("User Kosong");
        }
    }

    protected function checkout(){
        // $random = Str::random(10, 10000);
        // echo $random;
        $transData = $this->getData();
        $idTrans = $transData->idTrans;
        $user = $transData->user;
        $amt = $transData->amt;
        $fixedva = $transData->fixedva;
        
        $this->validateUser($user);

        $params = [
            'invoice' => Utils::invoiceCode($idTrans, 'INV'),
            'email' => 'topup.kepengpadang@gmail.com',
            'description' => 'Top up',
            'amount' => $amt,
        ];

        // $this->result = true;

        $dataInvoice = null;
        $invoice = new InvoiceTransact();
        if($fixedva){
            $params['invoice'] = str_replace("/", "-", $params['invoice']);
            $dataInvoice = $invoice->createInvoiceWithFVA($params, $fixedva);
        }else{
            $dataInvoice = $invoice->createInvoice($params);
        }
        //$this->result->id
        $dataInvoice['id_transaction'] = $idTrans; 
        $invoice->saveInvoice($dataInvoice);

        $this->result = $dataInvoice;
        // to public/ . dummy file will gitignored
        // toFile(presonRet($this->result),"dummyTrans");
        // toFile(presonRet($dataInvoice),"dummyInv");
    }

    protected function doProccess()
    {
        try {
            DB::beginTransaction();
            
            $this->checkout();

            DB::commit();

            DB::beginTransaction();
            // event(new CheckoutOccured($trans));
            DB::commit();

            return $this->result;
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e->getMessage());
            // $this->transactionResult = $e->getMessage();
            // preout($e->getMessage());
            return false;
        }
    }
}
