<?php

namespace App\Services\Transaction;

use App\Exceptions\XenQRCodeCreateFail;
use App\Helpers\Constants;
use App\Models\XenQR;
use App\Services\Abstracts\TransactionProcess;
use Illuminate\Support\Facades\DB;
use Xendit\Xendit;
use chillerlan\QRCode\QRCode;
// use App\Helpers\Dummy;

class QRCodeCreateTransact extends TransactionProcess
{
    /**
     * @var \App\Services\result|array
     */
    protected $cart;

    public function __construct()
    {
        parent::__construct();
        // $this->transaction = new Transaction();
        $this->xendit = Xendit::setApiKey(
            config('services.xendit.apisecretkey')
        );
    }

    protected function validate()
    {
        return true;
    }

    protected function verify()
    {
        return true;
    }

    protected function doProccess()
    {
        try {
            DB::beginTransaction();
            $transData = $this->getData();

            $params = [
                "external_id" => $transData["external_id"],
                "type" => "DYNAMIC", // to do : discuss
                "callback_url" => $transData["callback_url"], 
                    // to do : fix
                "amount" => $transData["amount"],
            ];

            $result = \Xendit\QRCode::create($params);

            //insert qrcode generated
            XenQR::create( XenQR::fromXendit($result) );
            
            // generate qr file

            DB::commit();

            return $result;
            // to do : specific exception
        } catch (XenQRCodeCreateFail $e) {
            DB::rollBack();
            $this->result = $e->getMessage();

            return false;
        } catch (\Exception $e) {
            DB::rollBack();
            $this->result = $e->getMessage();
            // preout($e->getMessage());
            return false;
        }
    }
}
