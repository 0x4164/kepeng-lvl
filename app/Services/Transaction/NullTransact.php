<?php

namespace App\Services\Transaction;

use App\Services\Abstracts\TransactionProcess;

class NullTransact extends TransactionProcess
{
    protected function validate()
    {
        /*
        validate
        - user
        - cart
         */
        return true;
    }

    protected function verify()
    {
        return true;
    }

    public function setPublicResult($result){
        $this->result = $result;
    }

    protected function doProccess()
    {
        try {
            return $this->getResult();
        // to do : specific exception
        } catch (\Exception $e) {
            DB::rollBack();
            // dd($e->getMessage());
            $this->result = $e->getMessage();
            preout($e->getMessage());
            return false;
        }

    }
}
