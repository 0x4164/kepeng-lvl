<?php

namespace App\Services\Transaction;

use App\Helpers\Constants;
use App\Models\XenQR;
use App\Services\Abstracts\TransactionProcess;
use Illuminate\Support\Facades\DB;
use Xendit\Xendit;
// use App\Helpers\Dummy;

class QRCodeCallbackTransact extends TransactionProcess
{
    /**
     * @var \App\Services\result|array
     */
    protected $cart;

    public function __construct()
    {
        parent::__construct();
        $this->xendit = Xendit::setApiKey(
            config('services.xendit.apisecretkey')
        );
    }

    protected function validate()
    {
        return true;
    }

    protected function verify()
    {
        return true;
    }

    protected function doProccess()
    {
        try {
            DB::beginTransaction();
            $data = 
                json_decode(file_get_contents('php://input'), true);
            // preout($data);
            toFile(json_encode($data), "dummyQRCallback");

            if (!empty($data)) {
                $xenqr = XenQR::where([
                    'external_id' => 
                        $data['external_id'], 'status' => 'ACTIVE'
                ])->first();

                $xenqr->update([
                    'status' => $data['status'],
                ]);
            }

            DB::commit();

            return $result;
            // to do : specific exception
        } catch (\Exception $e) {
            DB::rollBack();
            $this->result = $e->getMessage();
            // preout($e->getMessage());
            return false;
        }
    }
}
