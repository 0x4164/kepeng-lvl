<?php

namespace App\Services\Transaction;

use App\Models\Invoices;
use App\Models\XenFixedVA;
use Illuminate\Support\Facades\DB;
use Xendit\Xendit;

class InvoiceTransact
{
    private $xendit;

    public function __construct()
    {
        $this->xendit = Xendit::setApiKey(
            config('services.xendit.apisecretkey')
        );
    }

    public function setParams($req)
    {

    }

    public function createInvoice($req)
    {
        $params = [
            'external_id' => $req['invoice'],
            'payer_email' => $req['email'],
            'description' => $req['description'],
            'amount' => $req['amount'],
        ];

        $createInvoice = \Xendit\Invoice::create($params);
        return $createInvoice;
    }
    
    public function createInvoiceWithFVA($req, $fixedva)
    {
        $fixedva = XenFixedVA::where("account_number", $fixedva)->first()->toArray();
        
        if($fixedva['status'] !== "ACTIVE"){
            throw new \Exception("Virtual account belum aktif");
        }

        //update va, expected_amount,, then create invoice
        $updateParams = ["expected_amount" => $req['amount']];
        $updateVA = \Xendit\VirtualAccounts::update($fixedva['xen_id'], $updateParams);

        $params = [
            'external_id' => $req['invoice'],
            'payer_email' => $req['email'],
            'description' => $req['description'],
            'amount' => $req['amount'],
            'callback_virtual_account_id' => $fixedva['xen_id'],
        ];

        $createInvoice = \Xendit\Invoice::create($params);

        return $createInvoice;
    }

    public function saveInvoice($req)
    {
        return Invoices::create([
            'id_invoice' => $req['id'],
            'invoice_url' => $req['invoice_url'], // save url for redirect / reference
            'id_transaction' => $req['id_transaction'],
            'external_id' => $req['external_id'],
            'user_id' => $req['user_id'],
            'is_high' => null,
            'payment_method' => "dummy",
            'status' => $req['status'],
            'merchant_name' => $req['merchant_name'],
            'amount' => $req['amount'],
            // err formatting
            'expiry_date' => $req['expiry_date'],
            'paid_amount' => 0,
            'bank_code' => null,
            'paid_at' => null,
            'payer_email' => $req['payer_email'],
            'description' => $req['description'],
            'adjusted_received_amount' => 0,
            'fees_paid_amount' => 0,
            'updated' => $req['updated'],
            'created' => $req['created'],
            'currency' => $req['currency'],
            'payment_channel' => null,
            'payment_destination' => null,
        ]);
    }
}
