<?php

namespace App\Services\Transaction;

use Carbon\Carbon;
use App\Helpers\Constants;
use App\Models\Invoices;
use App\Models\Payouts;
use App\Models\XenFixedVA;
use App\Services\Abstracts\TransactionProcess;
use App\Pencairan;
use App\User;
use App\top_up_history;
use App\Helpers\FCM;
// use App\Helpers\Dummy;
use Illuminate\Support\Facades\DB;

class PayCallbackTransact extends TransactionProcess
{
    /**
     * @var \App\Services\result|array
     */
    protected $cart;
    protected $fcm;
    
    protected $useFVA = false;

    public function __construct()
    {
        parent::__construct();
        //$this->fcm = new FCM;
    }

    protected function validate()
    {
        /*
        validate
        - user
        - cart
         */
        return true;
    }

    protected function verify()
    {
        return true;
    }

    public function getUseFVA(){
		return (bool) $this->useFVA;
	}

    // use this is callback is from fixed va
	public function setUseFVA($useFVA){
		$this->useFVA = (bool) $useFVA;

		return $this;
	}

    // update kepeng
    protected function updateTopup($invoice){
        $invoice->topup()->update([
            'verifikator' => "xendit",
            'status' => Constants::KEPENG_STATUS_VERIFIED
        ]);

        if($this->getUseFVA()){
            // $ex = explode('/',$invoice->external_id);
            // $topup = top_up_history::where($ex[2])->first();
        }else{
            $ex = explode('/',$invoice->external_id);
            
            try{
                $topup = top_up_history::whereKodeUnik($ex[2])->first();
            }catch(\Exception $e){
                $ex = explode('-',$invoice->external_id);
                $topup = top_up_history::whereKodeUnik($ex[2])->first();
            }

        }
        if($topup){
        //add saldo
            $user = User::find($topup->user_id);
            if($user){
                $user->saldo += $topup->kepeng;
                $user->save();
            }
        }
    }

    protected function withVA($invoice, $data){
        $invoice->update([
            'status' => Constants::TRANS_STATUS_PAID,
            'merchant_name' => $data['merchant_code'],
            'amount' => $data['amount'],
            'paid_amount' => $data['amount'],
            'bank_code' => $data['bank_code'],
            'paid_at' => Carbon::now(),
            'payment_channel' => "FVA",
            'payment_method' => $data['payment_id'].":".$data["callback_virtual_account_id"]
        ]);
    }

    protected function withoutVA($invoice, $data){
        $paidAmt = arrayGet($data, 'paid_amount') ? arrayGet($data, 'paid_amount') : 0;
        $bankCode = arrayGet($data, 'bank_code') ? arrayGet($data, 'bank_code') : "err";
        $adjusted_received_amount = arrayGet($data, 'adjusted_received_amount') ? arrayGet($data, 'adjusted_received_amount') : 0;
        $payment_channel = arrayGet($data, 'payment_channel') ? arrayGet($data, 'payment_channel') : 0;
        
        $invoice->update([
            'status' => $data['status'],
            'is_high' => $data['is_high'],
            'merchant_name' => $data['merchant_name'],
            'amount' => $data['amount'],
            'paid_amount' => $paidAmt,
            'bank_code' => $bankCode,
            'paid_at' => Carbon::now(),
            'adjusted_received_amount' => $adjusted_received_amount,
            'payment_channel' => $payment_channel
        ]);

        // if status paid execute this
        if ($data['status'] == Constants::TRANS_STATUS_PAID) {
            $this->updateTopup($invoice);
        }
    }

    protected function doProccess()
    {
        try {
            DB::beginTransaction();

            $data = json_decode(file_get_contents('php://input'), true);
            
            if (!empty($data)) {

                // with va
                if($this->getUseFVA()){
                    $fva = XenFixedVA::where("account_number", 
                            $data['merchant_code'].$data['account_number']
                        )->first();

                    $user = $fva->user;
                    $topups = $user->topups;

                    $topup = $topups->where("total", $data['amount'])
                        ->where("status", 0)->first();
                    if($topup){
                        $topup->update([
                            'verifikator' => "xendit",
                            'status' => Constants::KEPENG_STATUS_VERIFIED
                        ]);
                        $invoice = $topup->invoice;
                        if($invoice){
                            $this->withVA($invoice, $data);
                            $user->saldo += abs($topup->kepeng);
                            $user->save();
                        }else{
                            throw new \Exception("Invoice not found");
                        }
                    }
                
                // without va
                }else{
                    $invoice = Invoices::where([
                        'external_id' => $data['external_id'], 
                        'status' => 'PENDING'
                    ])->first();
                    if($invoice){
                        $this->withoutVA($invoice, $data);
                    }else{
                        $data = null;
                        $this->setCode(400)
                            ->setMessage("Invoice tidak ketemu / sudah terbayar");
                    }
                }

                DB::commit();
            }

            return $data;
            // to do : specific exception
        } catch (\Exception $e) {
            DB::rollBack();
            // dd($e->getMessage());
            throw $e;
            $this->result = $e->getMessage();
            // preout($e->getMessage());
            return false;
        }

    }
}
