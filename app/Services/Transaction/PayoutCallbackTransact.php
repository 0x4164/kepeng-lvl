<?php

namespace App\Services\Transaction;

use App\Helpers\Constants;
use App\Models\Disbursement;
use App\Services\Abstracts\TransactionProcess;
use App\Pencairan;
use App\User;
// use App\Helpers\Dummy;
use Illuminate\Support\Facades\DB;

class PayoutCallbackTransact extends TransactionProcess
{
    /**
     * @var \App\Services\result|array
     */
    protected $cart;
    protected $fcm;

    public function __construct()
    {
        parent::__construct();
        //$this->fcm = new FCM;
    }

    protected function validate()
    {
        /*
        validate
        - user
        - cart
         */
        return true;
    }

    protected function verify()
    {
        return true;
    }

    protected function doProccess()
    {
        try {
            DB::beginTransaction();
            // to do : get transaction id by amount paid's last digit

            $data = json_decode(file_get_contents('php://input'), true);
            // toFile(json_encode($data), "dummyInvCallback");
            
            // return;
            if (!empty($data)) {
                $xdb = Disbursement::where([
                    'external_id' => $data['external_id'], 'status' => 'PENDING'
                ])->first();

                if($xdb){
                    $xdb->status = $data['status'];
                    $xdb->save();

                    $ex = explode('/',$xdb->external_id);
                    $payout = Pencairan::whereKode($ex[2])->first();
                    $payout->verifikator = 'xendit';

                    // if status paid execute this
                    if ($data['status'] == 'COMPLETED') {
                        $payout->status = Constants::KEPENG_STATUS_VERIFIED;
                        $payout->save();

                        //get token fcm
                        // $user = User::find($invoice->user_id);
                        // if($user){
                        //     $this->fcm->setToken($user->token_fcm)->send('Berhasil topup melalui xendit dengan kode '.$invoice->topup()->kode_unik);   
                        // }
                    }
                    if($data['status'] == 'FAILURE' || $data['status'] == 'FAILED'){
                        $payout->status = Constants::KEPENG_STATUS_REJECT;
                        $payout->save();
                        //saldo balikin
                        $user = User::find($payout->user_id);
                        if($user){
                            $user->saldo+= $payout->pencairan;
                            $user->save();
                        }
                    }
                }else{
                    $data = null;
                    $this->setCode(400)
                        ->setMessage("Pencairan tidak ketemu / sudah terbayar");
                }

                DB::commit();
            }


            return $data;
            // to do : specific exception
        } catch (\Exception $e) {
            DB::rollBack();
            // dd($e->getMessage());
            $this->result = $e->getMessage();
            // preout($e->getMessage());
            return false;
        }

    }
}
