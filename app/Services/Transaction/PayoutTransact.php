<?php

namespace App\Services\Transaction;

use App\Helpers\Constants;
use App\Helpers\Utils;
use App\Models\Invoices;
use App\Models\Payouts;
use App\Services\Abstracts\TransactionProcess;
use Illuminate\Support\Facades\DB;
use Xendit\Xendit;
use App\Models\Disbursement;
// use App\Helpers\Dummy;

class PayoutTransact extends TransactionProcess
{
    /**
     * @var \App\Services\result|array
     */
    protected $cart;

    public function __construct()
    {
        parent::__construct();
        // $this->transaction = new Transaction();
        $this->xendit = Xendit::setApiKey(
            config('services.xendit.apisecretkey')
        );
    }

    protected function validate()
    {
        return true;
    }

    protected function verify()
    {
        return true;
    }

    protected function doProccess()
    {
        try {
            DB::beginTransaction();
            $transData = $this->getData();
            
            // to do : change data
            $amt = $transData->amt; // user input
            $user = $transData->user; // user input
            $idTrans = $transData->idTrans;

            $params = [
                'external_id' => Utils::invoiceCode($idTrans, 'PAY'),
                'amount' => $amt,
                'bank_code' => $transData->bank,
                'account_holder_name' => $transData->bank_holder,
                'account_number' => $transData->bank_account,
                'description' => 'Withdrawal kepeng',
            ];
        
            $xdb = \Xendit\Disbursements::create($params);

            // insert payout
            // $createPayout = Payouts::create(
            //     Payouts::fromXendit($createPayout)
            // );
            //create disbursement
            $disbursement = new Disbursement;
            $disbursement->external_id = \Arr::get($xdb,'external_id');
            $disbursement->user_id = \Arr::get($xdb, 'user_id');
            $disbursement->amount = \Arr::get($xdb, 'amount');
            $disbursement->bank_code = \Arr::get($xdb, 'bank_code');
            $disbursement->account_bank_holder = \Arr::get($xdb, 'account_holder_name');
            $disbursement->status = \Arr::get($xdb, 'status');
            $disbursement->save();
              
            DB::commit();

            return $disbursement;
            // to do : specific exception
        } catch (\Exception $e) {
            DB::rollBack();
            // dd($e->getMessage());
            $this->result = null;
            // preout($e->getMessage());
            return false;
        }
    }
}
