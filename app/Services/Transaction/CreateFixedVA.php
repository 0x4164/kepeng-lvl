<?php

namespace App\Services\Transaction;

use App\Helpers\Constants;
use App\Services\Abstracts\TransactionProcess;
use App\User;
use App\Models\XenFixedVA;
// use App\Helpers\Dummy;
use Illuminate\Support\Facades\DB;
use Xendit\Xendit;
use Xendit\Exceptions\ApiException;

class CreateFixedVA extends TransactionProcess
{
    /**
     * @var \App\Services\result|array
     */
    protected $cart;
    protected $fcm;

    public function __construct()
    {
        parent::__construct();
        
        $this->xendit = Xendit::setApiKey(
            config('services.xendit.apisecretkey')
        );
    }

    protected function validate()
    {
        return true;
    }

    protected function verify()
    {
        return true;
    }

    protected function getBank($type){
        $ret = null;
        $p = "4324";
        $dev = config('app.env') != "production" ;
        if($dev){
            $p = "9999";
        }

        switch($type){
            case "mandiri":
                $bc = "MANDIRI";
            break;
            case "bni":
                $bc = "BNI";
            break;
            case "bri":
                $bc = "BRI";
            break;
        }

        return [
            "bank_code" => $bc,
            "prefix" => $p,
        ];
    }

    protected function getBankPad($type, $number){
        switch($type){
            case "bri":
            case "mandiri":
                // $number = str_pad($number, 6, '0', STR_PAD_LEFT);
                $number = str_pad($number, 6, '0', STR_PAD_LEFT);
            break;
            case "bni":
                $number = str_pad($number, 8, '0', STR_PAD_LEFT);
            break;
            default:
                return $number;
        }

        return $number;
    }

    protected function createFVA($user, $bankType){
        $userid = $user->id;
        $userName = $user->name;

        $bank = $this->getBank($bankType);
        $userBank = $bank['bank_code'];
        $virtual_account_number = $bank['prefix'].$this->getBankPad($bankType, $user->id);

        $params = [
            "external_id" => (string) $userid,
            "bank_code" => $userBank,
            "name" => $userName,
            "virtual_account_number" => $virtual_account_number,
            "is_closed" => true,
            "expected_amount" => 1,
        ];

        $result = [
            // "payload" => null,
            "va" => null,
            "error" => null
        ];
        
        try {
            DB::beginTransaction();
            $newva = null;
            $createVA = \Xendit\VirtualAccounts::create($params);
            // $result["payload"] = $createVA;
            // toFile(presonRet($createVA),"dummyVa".$userBank);
            
            if($createVA){
                $newva = $user->xenFixedVa()->create(XenFixedVA::fromXendit($createVA));
                DB::commit();
                $result['va'] = $newva;
            }
    
            return $result;
        }catch(\Exception $e){
            DB::rollBack();

            $result['error'] = $e->getMessage();
            return $result;
        }
    }

    protected function createFvaByOption($type = null, $user){
        $data = [];
        switch($type){
            case "mandiri":
                $data["mandiri"] = $this->createFVA($user, "mandiri");
            break;
            case "bni":
                $data["bni"] = $this->createFVA($user, "bni");
            break;
            case "bri":
                $data["bri"] = $this->createFVA($user, "bri");
            break;
            case "full":
                $this->createByOption("mandiri");
                $this->createByOption("bni");
                $this->createByOption("bri");
            break;
        }

        return $data;
    }

    protected function doProccess()
    {
        try {
            // to do : change to personal data
            $transData = $this->getData();
            $user = $transData['user'];
            $data = [];
            if($user){
                $fvas = $user->fixedVas();
                foreach($fvas as $key => $fva){
                    // create fixed va if fixed va is null or empty
                    if(empty($fva)){
                        $fvas[$key] = $this->createFvaByOption($key, $user)[$key];
                    }else{
                        $fvas[$key] = null;
                    }
                }
                $data = $fvas;
            }
    
            return $data;

            // to do : specific exception
        } catch (\Exception $e) {
            // preout($e->getMessage());
            throw new \Exception("do process : ".$e->getMessage());
            
            return false;
        }

    }
}
