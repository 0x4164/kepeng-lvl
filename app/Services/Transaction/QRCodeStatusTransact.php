<?php

namespace App\Services\Transaction;

use App\Exceptions\XenQRCodeCreateFail;
use App\Helpers\Constants;
use App\Models\Invoices;
use App\Models\Payouts;
use App\Models\XenQR;
use App\Services\Abstracts\TransactionProcess;
use Illuminate\Support\Facades\DB;
use Xendit\Xendit;
// use App\Helpers\Dummy;

class QRCodeStatusTransact extends TransactionProcess
{
    /**
     * @var \App\Services\result|array
     */
    protected $cart;

    public function __construct()
    {
        parent::__construct();
        $this->xendit = Xendit::setApiKey(
            config('services.xendit.apisecretkey')
        );
    }

    protected function validate()
    {
        return true;
    }

    protected function verify()
    {
        return true;
    }

    protected function doProccess()
    {
        try {
            DB::beginTransaction();
            $transData = $this->getData();

            $external_id = $transData['external_id'];
            $result = \Xendit\QRCode::get($external_id);

            DB::commit();

            return $result;
            // to do : specific exception
        } catch (\Exception $e) {
            DB::rollBack();
            $this->result = $e->getMessage();
            // preout($e->getMessage());
            return false;
        }
    }
}
