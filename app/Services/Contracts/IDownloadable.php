<?php
namespace App\Services\Contracts;

interface IDownloadable{
    public function download();
}