<?php
namespace App\Services\Contracts;

interface IHasChart{
    public function getChart();
}