<?php
namespace App\Services\Contracts;

interface IChartModel{
    public function getAlias($where,$val);
    public function data($gby, $where, $val);
}