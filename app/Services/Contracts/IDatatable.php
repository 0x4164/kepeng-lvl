<?php
namespace App\Services\Contracts;

interface IDatatable{
    public function get();
    public function where();
}