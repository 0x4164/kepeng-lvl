<?php

namespace App\Services\Abstracts;

use App\Helpers\Constants;
use Illuminate\Http\Request;
use Auth;
use stdClass;

abstract class AbstractService
{
    private $user;

    public function __construct()
    {
        
    }

    public function setRequest($request){
        $this->request = $request;

        return $this;
    }

    public function getRequest(){
        return $this->request;
    }

    public function setUser($user = null){
        if($user){
            $this->user = $user;
        }else{
            $this->user = Auth::user();
        }

        return $this;
    }

    public function getUser(){
        return $this->user;
    }

    public function setData($data){
        $this->data = $data;

        return $this;
    }

    public function getData(){
        return $this->data;
    }
}
