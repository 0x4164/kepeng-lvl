<?php

namespace App\Services\Abstracts;

use Illuminate\Support\Facades\Log;
use Edujugon\Log\Log as ELog;
use App\Helpers\Constants;
use App\Services\Contracts\IProccess;

abstract class TransactionProcess extends Process{
    // use HasTransaction;

    protected $couriers;
    protected $carts;
    protected $code;
    protected $data;
    protected $msg;

    public function __construct(){
        // echo "init";
        $this->setMessage("Sukses")->setCode(200);
    }

    abstract protected function validate();
    abstract protected function verify();
    
    public function proccess()
    {
        if ($this->validate() && $this->verify()) {
            $result = $this->doProccess();
            if($result){
                $this->success();
                $this->setResult($result);

                return $this;
            }else{
                return $this->fail();
            }
        } else {
            return $this->fail();
        }
    }

    abstract protected function doProccess();
    
    protected function success(){
        $this->setCode(200);
        Log::channel(Constants::LOG_TRANSACTION)
            ->info(Constants::logFmt(get_class($this), "success", $this));
        return "success";
    }

    protected function fail(){
        $this->setCode(400);
        Log::channel(Constants::LOG_TRANSACTION)
            ->info(Constants::logFmt(get_class($this), "fail", $this));
        return $this->result;
    }

    protected function setCode($code){
        $this->code = $code;

        return $this;
    }

    public function getCode(){
        return $this->code;
    }

    protected function setResult($result){
        $this->result = $result;
    }

    public function getResult(){
        return $this->result;
    }

    protected function setMessage($msg){
        $this->msg = $msg;

        return $this;
    }

    public function getMessage(){
        return $this->msg;
    }

    public function setData($data){
        $this->data = $data;

        return $this;
    }

    public function getData(){
        return $this->data;
    }

    protected function err($msg = ""){
        throw new \Exception($msg);
    }
}
