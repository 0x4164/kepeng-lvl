<?php

namespace App\Services\Abstracts;

use App\Models\Cart;
use App\Models\Couriers;
use App\Services\Contracts\IProccess;
use App\Models\Transaction;
use App\Models\TransactionDetail;

abstract class Process implements IProccess{

    protected $result;

    public function __construct(){
        // echo "init";
    }

    abstract protected function validate();
    abstract protected function verify();
    abstract public function proccess();
    abstract protected function doProccess();
    abstract protected function success();
    abstract protected function fail();
}
