<?php

namespace App\Services;

use App\Services\Abstracts\AbstractService;
use App\Helpers\Constants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Auth;
use stdClass;

class KepengService extends AbstractService
{
    private $user;
    private $sendEmail;

    public function __construct()
    {
        $this->transSvc = new TransactionService();
        $this->setSendEmail(false);
    }

    public function getSendEmail(){
        return $this->sendEmail;
    }

    public function setSendEmail($sendEmail){
        $this->sendEmail = (bool) $sendEmail;

        return $this;
    }

    public static function generator($type){
        $ret = "";
        switch($type){
            case "topup-invoice-number":
                $check_data = DB::table('top_up_history')->get();
                $transaction_fee = DB::table('transaction_fee')
                    ->select('transaction_fee')->first()->transaction_fee;

                if ($check_data->isEmpty()) {
                    $kode_top_up = 0;
                } else {
                    $kode_top_up = DB::table('top_up_history')
                        ->select('kode_unik')->latest()->first()
                        ->kode_unik;
                }

                $int = (int) $kode_top_up + 1;
                $int =  (string) $int;
                $ret = str_pad($int, 3, '0', STR_PAD_LEFT);
            break;
        }
        return $ret;
    }

    public function topUp()
    {
        try {
            $kode_top_up = 0;

            $transaction_fee = DB::table('transaction_fee')
            ->select('transaction_fee')->first()->transaction_fee;
            
            $user = $this->getUser();
            $request = $this->getRequest();
            
            $invID = self::generator("topup-invoice-number");
            // preson($invID);
            // exit;
            // $kode_unik = sprintf('%03d', rand(1, 999));
            $amt = abs($request->kepeng) + abs($request->fee);
            $fva = $request->fixedva;

            $data = new stdClass();
            $data->idTrans = $invID;
            $data->user = $user;
            $data->amt = $amt;
            $data->fixedva = $fva;

            $checkout = $this->transSvc
                ->setData($data)
                ->setType(Constants::TRANS_CHECKOUT)
                ->proccess();

            if ($checkout) {
                $top_up = DB::table('top_up_history')->insert(
                    [
                        'kode_unik'   => $invID,
                        'user_id'     => $user->id,
                        'nama'        => $user->name,
                        'no_hp'        => $user->no_hp,
                        'kepeng'      => abs($request->kepeng),
                        'transaction_fee' => abs($request->fee),
                        'total'        => $amt,
                        'foto_bukti'  => "belum ada",
                        'created_at'  => \Carbon\Carbon::now(),
                        'updated_at'  => \Carbon\Carbon::now(),
                    ]
                );

                // if($this->getSendEmail()){
                //     $data =  Mail::send('email', [
                //         'nama' => "lian",
                //         'pesan' => "tes"
                //     ], function ($message) use ($request, $user, $invID) {
        
                //         $subject = "Top up saldo dengan kode " 
                //             . $invID . ", oleh " . $user->name 
                //             . " (" . $user->no_hp . ")" 
                //             .  ", Sebesar Rp. " 
                //             . number_format($request->kepeng, 2, ',', '.');
                        
                //         $message->subject($subject);
                //         $message->from('kepengpadang@gmail.com', 'Kepeng_padang');
                //         $message->to('kepengpadang@gmail.com');
                //     });
                // }

                return [
                    'succes' => true,
                    'error' => '',
                    'message' => 'berhasil',
                    'kode_unik' => $invID,
                    'top_up' => $request->kepeng,
                    'transaction_fee' => $transaction_fee,
                    'total' => $request->kepeng,
                    'checkout' => $checkout->result->getResult()
                ];
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function withDraw(){
        try {
            $kode_wd = 0;
            $check_data = DB::table('pencairan')->get();
            $transaction_fee = 9500;

            $user = $this->getUser();
            $request = $this->getRequest();

            if ($check_data->isEmpty()) {
                $kode_wd = 0;
            } else {
                $kode_wd = DB::table('pencairan')
                    ->select('kode')->latest()->first()
                    ->kode;
            }

            $int = (int) $kode_wd + 1;
            $int =  (string) $int;
            $invID = str_pad($int, 3, '0', STR_PAD_LEFT);

            // if($this->getSendEmail()){
            //     $data =  Mail::send('email', [
            //         'nama' => "lian",
            //         'pesan' => "tes"
            //     ], function ($message) use ($request, $invID, $user) {
    
            //         $subject = "Withdraw saldo dengan kode " 
            //             . $invID . ", oleh " . $user->name 
            //             . " (" . $user->no_hp . ")" 
            //             .  ", Sebesar Rp. " 
            //             . number_format($request->kepeng, 2, ',', '.');
                    
            //         $message->subject($subject);
            //         $message->from('kepengpadang@gmail.com', 'Kepeng_padang');
            //         $message->to('kepengpadang@gmail.com');
            //     });
            // }

            // $kode_unik = sprintf('%03d', rand(1, 999));
            $amt = abs( $request->pencairan) - abs($transaction_fee);

            $top_up = DB::table('pencairan')->insert(
                [
                    'kode'   => $invID,
                    'user_id'     => $user->id,
                    'user_nama'        => $user->name,
                    'no_hp'        => $user->no_hp,
                    'pencairan'      => abs($request->pencairan),
                    'biaya_admin' => abs($transaction_fee),
                    'total_terima'        => $amt,
                    'created_at'  => \Carbon\Carbon::now(),
                    'updated_at'  => \Carbon\Carbon::now(),
                    'status' => 0
                ]
            );

            $data = new stdClass();
            $data->idTrans = $invID;
            $data->user = $user;
            $data->amt = $amt;
            $data->bank = $request->bank;
            $data->bank_holder = $request->atas_nama;
            $data->bank_account = $request->norek;

            $payout = $this->transSvc
                ->setData($data)
                ->setType(Constants::TRANS_PAYOUT)
                ->proccess();
            
            //kurangi saldo
            $user = Auth::user();
            $user->saldo -= $request->pencairan;
            $user->save();

            if ($top_up) {
                return [
                    'succes' => true,
                    'error' => '',
                    'message' => 'berhasil',
                    'kode_unik' => $invID,
                    'withdraw' => $request->pencairan,
                    'admin_fee' => $transaction_fee,
                    'total' => $amt,
                    'checkout' => $payout->result->getResult()
                ];
            }
        } catch (\Illuminate\Database\QueryException $th) {
            dd($th);
        }
    }
}
