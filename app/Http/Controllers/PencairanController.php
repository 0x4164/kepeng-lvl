<?php

namespace App\Http\Controllers;

use App\Pencairan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\DB;
use App\Services\KepengService;
class PencairanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pencairan = Pencairan::latest()->where('status',0)->get();
      
        if ($request->ajax()) {
            return datatables()->of($pencairan)
                ->addColumn('action', 'admin.topup.action')
               
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.pencairan.index', compact('pencairan'));
    }

    public function history(Request $request)
    {
        $pencairan = Pencairan::latest()->whereIn('status',[1,2])->get();
      
        if ($request->ajax()) {
            return datatables()->of($pencairan)
                ->addColumn('action', 'admin.topup.action')
                ->editColumn('status', function ($row) {
                    if ($row->status == 1) {
                        return '<h6 class="badge badge-info">disetujui</h6>';
                    }
                    if ($row->status == 2) {
                        return '<h6 class="badge badge-secondary">dibatalkan</h6>';
                    }
                })
                ->rawColumns(['action','status'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.pencairan.history', compact('pencairan'));
    }


    public function apiHistory(Request $request)
    {
        $pencairan = Pencairan::latest()->where('user_id',Auth::user()->id)->get();
        
        return response()->json(
            [
                'succes' => true,
                'error' => '',
                'data' => $pencairan
            ]);
    }

    public function verifikasi(Request $request)
    {

         // Begin Transaction
   DB::beginTransaction();

   try {
            $total_pencairan = $request->pencairan2+ $request->biaya_admin2;
            $user_saldo      = User::where('id', $request->user_id2)->first()->saldo;
        //    dd( $user_saldo ." + ".$total_pencairan . "=".  ($user_saldo-$total_pencairan));

            $pencairan = Pencairan::where('kode', $request->kode2)
            ->update([
                'status' => 1,
                'verifikator'=>Auth::user()->name,
                'updated_at'=>Carbon::now()
                ]);

            // $user = User::where('id', $request->user_id2)
            // ->update([ 'saldo' =>  $user_saldo - $total_pencairan ]);

            $user_token_fcm = DB::table('users')
            ->where('id', '=', $request->user_id2)->first()->token_fcm;

            DB::commit();
                
            Alert::success('Sukses', 'Berhasil verifikasi Pencairan');
            $msg = array(
                'body'  => 'Pencairan anda dengan kode ' . $request->kode2 . ' telah diverifikasi oleh admin',
                'title'    => 'Kepeng'
            );

            $fields = array(
                'to' => $user_token_fcm,
                'notification'    => $msg
            );
            $headers = array(
                'Authorization: key=AAAA6priZMM:APA91bE5kIppJ2Rh3bGfAoKiNJOoB1JGqBZTLJT90u900xrAp2xsnXdmfO5vh7rpdPyBp6aAEhS2QsJx-xiCDSNl6aDkqllBv7LtB7XgKJgkPmEb4Tpm7TosKOtnUVAMxim8QUIl-7XV',
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            return redirect()->back();
   } catch (\Throwable $th) {
        Alert::error('Gagal', 'Terjadi kesalahan'.$th);
        return redirect()->back();
        DB::rollback();
   }

     

    }

    public function reject(Request $request)
    {


        try {
            $pencairan = 
            Pencairan::where('kode', $request->kode)
            ->update(['status' => 2 , 
              'verifikator'=>Auth::user()->name,
              'updated_at'=>Carbon::now()
              ]);

            //mengembalikan saldo withdraw
            $user = User::findOrFail($request->user_id);
            $user->saldo += $pencairan->total_terima;
            $user->save();
    
            $user_token_fcm = $user->token_fcm;
    
            Alert::success('Sukses', 'Berhasil Reject Pencairan');
            $msg = array(
                'body'  => 'Pencairan anda dengan kode ' . $request->kode . ' telah dibatalkan oleh admin',
                'title'    => 'Kepeng'
            );
    
            $fields = array(
                'to' => $user_token_fcm,
                'notification'    => $msg
            );
            $headers = array(
                'Authorization: key=AAAA6priZMM:APA91bE5kIppJ2Rh3bGfAoKiNJOoB1JGqBZTLJT90u900xrAp2xsnXdmfO5vh7rpdPyBp6aAEhS2QsJx-xiCDSNl6aDkqllBv7LtB7XgKJgkPmEb4Tpm7TosKOtnUVAMxim8QUIl-7XV',
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::error('Gagal', 'Terjadi kesalahan'.$th);
            return redirect()->back();
         
        }
     

    }

    public function apiKirimPencairan(Request $request){

        try {
            $kode_pencairan = 0;

            $check_data = DB::table('pencairan')->get();
            $user_id = DB::table('users')
            ->where('no_hp', '=', $request->no_hp)->first()->id;
            $user_saldo = DB::table('users')
            ->where('no_hp', '=', $request->no_hp)->first()->saldo;
           
            if ($check_data->isEmpty()) {
                $kode_pencairan = 0;
            } else {
                $kode_pencairan = DB::table('pencairan')->select('kode')->latest()->first()->kode;
                
            }
    
            $int = (int) $kode_pencairan + 1;
            $int =  (string) $int;
            $invID = str_pad($int, 3, '0', STR_PAD_LEFT);

            if($user_saldo < ($request->pencairan + $request->biaya_admin)){
                return response()->json([
                    'succes' => false,
                    'error' => '',
                    'message' => 'Maaf saldo anda tidak cukup'
                ]);
            }else if($user_saldo < ($request->pencairan)){
                return response()->json([
                    'succes' => false,
                    'error' => '',
                    'message' => 'Maaf saldo anda tidak cukup',
                ]);
            }
            else if($request->pencairan < ($request->biaya_admin)){
                return response()->json([
                    'succes' => false,
                    'error' => '',
                    'message' => 'Jumlah penarikan min Rp. 10.000',
                ]);
            }
            else{
                $pencairan = new Pencairan([
                    "kode"          => $invID,
                    "user_id"       => $user_id,
                    "user_nama"     => $request->user_nama,
                    "no_hp"         => $request->no_hp,
                    "pencairan"     => $request->pencairan,
                    "biaya_admin"   => $request->biaya_admin,
                    "total_terima"  => $request->pencairan - $request->biaya_admin,
                    "status" => 0
                ]);
                $pencairan->save();
                
                return response()->json([
                    'succes' => true,
                    'error' => '',
                    'message' => 'berhasil',
                ]);
            }
    
        } catch (\Throwable $th) {
            return response()->json([
                'succes' => false,
                'error' => $th,
                'message' => 'gagal'
            ]);
        }

       

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pencarianRequest(Request $request){
        $kpgService = new KepengService();

        if(!$request->pencairan || !is_numeric($request->pencairan) || !$request->atas_nama || !$request->norek || !$request->bank){
            return response()->json([
                'succes' => false,
                'message' => 'Pencairan kosong, data bank kosong atau format salah'
            ]);
        }

        //cek saldo
        $saldo = Auth::user()->saldo;
        if($saldo < ($request->pencairan)){
            return response()->json([
                'succes' => false,
                'message' => 'Saldo tidak mencukupi, maximal pencairan Rp. '.number_format($saldo, 0, ',', '.')
            ]);
        }

        $return = $kpgService
            ->setRequest($request)->setUser()
            ->withDraw();
        return response()->json($return);
    }
}
