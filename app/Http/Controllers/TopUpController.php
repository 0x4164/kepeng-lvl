<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Services\KepengService;

class TopUpController extends Controller
{
    public function __construct()
    {
        $this->kepengSvc = new KepengService();
    }

    public function store(Request $request)
    {
        try{
            $return = $this->kepengSvc
                ->setRequest($request)->setUser()
                ->topUp();
            
            return jsonResponseStatus(true, "Ok", $return);
        } catch (\Exception $e) {
            return jsonResponseStatus(false, "Error : ".$e->getMessage());
        }
    }

    public static function quickRandom($length = 3)
    {
        $pool = '0123456789';
        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }
}
