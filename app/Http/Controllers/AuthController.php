<?php

namespace App\Http\Controllers;

use QrCode;
use Validator;
use App\User;
use App\Kirim_history;
use Carbon\Carbon;
use App\Helpers\Constants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Traits\TransactionTrait;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;


class AuthController extends Controller
{
    use TransactionTrait;
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function register(Request $request)
    {
        // $request->validate([
        //     'name' => 'required|string',

        //     'password' => 'required|string|confirmed'
        // ]);
        // $user = new User([
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'password' => bcrypt($request->password),
        //     'pin'       =>     Hash::make($request->pin),
        //     'no_hp'       =>   $request->no_hp,
        // ]);
        // $user->save();

        DB::table('users')->insert(
            [
                'name'      =>    $request->username,
                'no_hp'      =>    $request->no_hp,
                'pin'       =>     Hash::make($request->pin),
                'hak_akses_id'       =>  3,
                'created_at'      => \Carbon\Carbon::now(),
            ]
        );

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }


    public function custom_register(Request $request)
    {
        try{
            $this->initTransSvc();
            $cek_user = DB::table('users')->select('no_hp')->where('no_hp' , $request->no_hp)->get();
            
            if($cek_user->isEmpty()){
                // DB::table('users')->insert(
                $all = $request->all();
                $request = (object) $all;
                
                $user = User::create([
                    'name'         => $request->name,
                    'no_hp'        => $request->no_hp,
                    'username'        => $request->no_hp,
                    'pin'          => Hash::make($request->pin),
                    'hak_akses_id' => 3,
                    'tmpt_lahir'   => $request->tmpt_lahir,
                    'tgl_lahir'    => $request->tgl_lahir,
                    'lokasi'       => $request->lokasi,
                    'token_otp'    => $request->token_otp,
                    'created_at'   => \Carbon\Carbon::now(),
                ]);

                $data["user"] = $user;
                $ret = $this->doProccess(Constants::TRANS_CREATE_FVA, $data);

                return response()->json([
                    'succes' => true,
                    'error' => '',
                    'message' => 'Berhasil mendaftar'
                ]);
            }else{
                return response()->json([
                    'succes' => false,
                    'error' => '',
                    'message' => 'No Hp ini sudah terdaftar, gunakan no hp lain'
                ]);
            }
            
    
        } catch (\Throwable $th) {
            return response()->json([
                'succes' => false,
                'error' => '',
                'message' => 'Terjadi kesalahan'.$th
            ]);
        }
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'no_hp' => 'required|string',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        // dd(Hash::make('123'));

        try {
            $this->initTransSvc();
            $credentials = request(['no_hp', 'password']);
            if (!Auth::attempt($credentials))
                return response()->json([
                    'succes' => false,
                    'error' => '',
                    'message' => 'Username atau password salah'
                ]);

            // DB::beginTransaction();

            $user = $request->user();
            if ($user->active == 0) {
                return response()->json([
                    'succes' => false,
                    'message' => 'maaf akun anda telah di suspend oleh admin, hubungi admin untuk mengaktifkan kembali'
                ]);
            }

            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();

            DB::table('users')
                ->where('id', Auth::user()->id)
                ->update(['token_fcm' => $request->token_fcm,]);
            
            // create if user have not having fva
            $user = $request->user();
            $data["user"] = $user;
            $ret = $this->doProccess(Constants::TRANS_CREATE_FVA, $data);
            $user = auth()->user();

            return response()->json([
                'succes' => true,
                'user' => $user,
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'succes' => false,
                'message' => $th->getMessage()
            ]);
        }
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'succes' => true,
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function updatePhoto(Request $request)
    {

        $thumbnail_name = null;
        if ($request->hasFile('photo')) {
            $request->file('photo')->store('/upload/profil_users'); //store to storage link
            $thumbnail_name = $request->photo->hashName(); // get name only
        }
        $photo = DB::table('users')
            ->where('id', Auth::user()->id)
            ->update(
                [
                    'foto'   => $thumbnail_name,
                ]
            );
        return response()->json([
            'succes' => true,
            'message' => 'sukses mengganti photo'
        ]);
    }

    public function updateProfilData(Request $request)
    {

        $profil = DB::table('users')
            ->where('id', Auth::user()->id)
            ->update(
                [
                    'name'         => $request->name,
                    'tmpt_lahir'   => $request->tmpt_lahir,
                    'tgl_lahir'    => $request->tgl_lahir,
                    'lokasi'       => $request->lokasi,
                    'updated_at'   => \Carbon\Carbon::now(),
                ]
            );
        return response()->json([
            'succes' => true,
            'message' => 'sukses mengupdate profil'
        ]);
    }

    public function updatePass(Request $request)
    {

        if (Hash::check($request->password_old, Auth::user()->pin)) {
            $user = DB::table('users')
                ->where('id', Auth::user()->id)
                ->update(
                    [
                        'pin'         => Hash::make($request->password_new),
                    ]
                );
            return response()->json([
                'succes' => true,
                'message' => 'Sukses Ganti Password'
            ]);
        } else {
            return response()->json([
                'succes' => false,
                'message' => 'Password lama tidak cocok'
            ]);
        }
    }
    
    public function cekDaftar(Request $request){
        $no_hp = $request->no_hp;
          $user= DB::table('users')->where('no_hp',  $no_hp)->get();
          if($user->count()>0){
              
                return response()->json([
                    'succes' => false,
                    'message' => "User sudah terdaftar"
                ]);
            
          }
          else{
               return response()->json([
                    'succes' => true,
                    'message' => "User belum terdaftar"
                ]);
          }
       
    }
}
