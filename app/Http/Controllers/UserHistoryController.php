<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserHistoryController extends BaseController
{
    public function historyTopUp() {
        $history =  DB::table('top_up_history')
            ->where('user_id', '=', Auth::user()->id)
            ->orderBy('updated_at', 'desc')
            ->get();

        return response()->json( [
                'succes' => true,
                'error' => '',
                'data' => $history
            ]
        );
    }

    public function topupDetail($id){
        try{
            $user = Auth::user();
            $topup = $user->topups()->where("id", $id)->first();
            if($topup){
                $topup->invoice;
        
                return $this->jsonSuccessResponse($topup, "ok");
            }else{
                return $this->jsonFailResponse(null, "Top up tidak ada");
            }
        }catch(\Exception $e){
            return $this->jsonFailResponse(null, $e->getMessage());
        }
    }

    public function historyTransfer()
    {


        $history =  DB::table('kirim_history')
            ->orderBy('created_at', 'desc')
            ->where('id_pengirim', '=',  Auth::user()->id)->get();

        return response()->json(
            [
                'succes' => true,
                'error' => '',
                'data' => $history
            ]
        );
    }

    public function historyPenarikanPoint()
    {
        $history =  DB::table('top_up_admin_history')
            ->where('id_user', '=',  Auth::user()->id)
            ->where('aksi', '=', 'pengurangan')
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json(
            [
                'succes' => true,
                'error' => '',
                'data' => $history
            ]
        );
    }

    public function historyMintaUang()
    {

        $history =  DB::table('kirim_history')
            ->orderBy('created_at', 'desc')
            ->where('id_penerima', '=',  Auth::user()->id)->get();

        return response()->json(
            [
                'succes' => true,
                'error' => '',
                'data' => $history
            ]
        );
    }
}
