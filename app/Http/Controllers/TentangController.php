<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class TentangController extends Controller
{


    public function index()
    {
        return view('admin.tentang.index');
    }

    public function panduan()

    {
        $content = DB::table('tentang')
            ->where('title', '=', 'panduan')
            ->get();

        return view('admin.tentang.panduan', compact('content'));
    }


    public function syarat()

    {
        $content = DB::table('tentang')
            ->where('title', '=', 'syarat')
            ->get();

        return view('admin.tentang.syarat', compact('content'));
    }

    public function kebijakan()

    {
        $content = DB::table('tentang')
            ->where('title', '=', 'kebijakan')
            ->get();

        return view('admin.tentang.kebijakan', compact('content'));
    }

    public function bantuan()

    {
        $content = DB::table('tentang')
            ->where('title', '=', 'bantuan')
            ->get();

        return view('admin.tentang.bantuan', compact('content'));
    }




    public function storePanduan(Request $request)
    {
        DB::table('tentang')
            ->where('title', 'panduan')
            ->update(
                [
                    'content' => $request->content
                ]
            );

        Alert::success('Sukses', 'berhasil disimpan');
        return redirect()->back();
    }




    public function storeSyarat(Request $request)
    {
        DB::table('tentang')
            ->where('title', 'syarat')
            ->update(
                [
                    'content' => $request->content
                ]
            );

        Alert::success('Sukses', 'berhasil disimpan');
        return redirect()->back();
    }


    public function storeKebijakan(Request $request)
    {
        DB::table('tentang')
            ->where('title', 'kebijakan')
            ->update(
                [
                    'content' => $request->content
                ]
            );

        Alert::success('Sukses', 'berhasil disimpan');
        return redirect()->back();
    }


    public function storeBantuan(Request $request)
    {
        DB::table('tentang')
            ->where('title', 'bantuan')
            ->update(
                [
                    'content' => $request->content
                ]
            );

        Alert::success('Sukses', 'berhasil disimpan');
        return redirect()->back();
    }



    public function viewPanduan()
    {
        $content = DB::table('tentang')
            ->where('title', '=', 'panduan')
            ->get();

        return view('admin.tentang.panduan_view', compact('content'));
    }
    public function viewSyarat()
    {
        $content = DB::table('tentang')
            ->where('title', '=', 'syarat')
            ->get();

        return view('admin.tentang.panduan_view', compact('content'));
    }
    public function viewKebijakan()
    {
        $content = DB::table('tentang')
            ->where('title', '=', 'kebijakan')
            ->get();

        return view('admin.tentang.panduan_view', compact('content'));
    }
    public function viewBantuan()
    {
        $content = DB::table('tentang')
            ->where('title', '=', 'bantuan')
            ->get();

        return view('admin.tentang.panduan_view', compact('content'));
    }
}
