<?php
namespace App\Http\Controllers\Traits;

use App\Services\TransactionService;

trait TransactionTrait{
    public function initTransSvc(){
        $this->transSvc = new TransactionService();
    }

    public function doProccess($type, $data){
        $proc = $this->transSvc
            ->setData($data)->setType($type)
            ->proccess();
        $ret = $proc;
        return jsonResponse($proc->code, 
            $proc->message, $ret->result->getResult());
    }
}