<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class PengaturanController extends Controller
{
    public function fee()
    {
        $fee = DB::table('transaction_fee')->first();
        // dd($fee);
        return view('admin.pengaturan.fee', compact('fee'));
    }

    public function updateFee(Request $request)
    {

        $affected = DB::table('transaction_fee')
            ->where('id', 1)
            ->update(['transaction_fee' => $request->fee]);


        Alert::success('Sukses', 'Berhasil memperbarui transaction fee');
        return redirect()->back();
    }
}
