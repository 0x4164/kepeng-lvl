<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostStoreRequest;
use App\Sliders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class SliderController extends Controller
{
    const SLIDER_PATH = 'storage';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $sliders = DB::table('sliders')->select()->get();

        if ($request->ajax()) {

            $slider =  Sliders::all();
            return datatables()->of($slider)
                ->editColumn('img', function (Sliders $sliders) {
                    return '<img src="' .  $sliders->getThumbnail() . '" height="100px" width="200px" >';
                })
                ->addColumn('action', 'admin.slider.action')
                ->addIndexColumn()
                ->rawColumns(['img', 'action']) // wajib untuk menmapilkan memproses html misal gambar
                ->make(true);
        }

        return view('admin.slider.index', compact(Sliders::all()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $sliders = DB::table('sliders')->select()->get();

            return view('admin.slider.create', compact('sliders'));
        } catch (\Throwable $th) { }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostStoreRequest $request)
    {
        $thumbnail_name = null;
        if ($request->hasFile('thumbnail')) {
            //  $request->file('thumbnail')->store('/storage/upload/'); //store to storage link
            $thumbnail_name = $request->thumbnail->hashName(); // get name only
            // $path = Storage::putFile('/',$request->file('thumbnail'));
            $destinationPath = public_path(SELF::SLIDER_PATH);
            $file = $request->file('thumbnail');
            $file->move($destinationPath, $thumbnail_name);
        }

        DB::table('sliders')->insert(
            [
                'title'       => $request->title,
                'content'     => $request->content,
                'img'         => $thumbnail_name,
                'created_at'   => \Carbon\Carbon::now()
            ]
        );
        


        Alert::success('Sukses', 'berhasil menambahkan slider');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.javascript:;
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $sliders =  DB::table('sliders')->where('id', $id)->first();


        return view('admin.slider.edit', compact('sliders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostStoreRequest $request, $id)
    {
        
         $slider =  Sliders::find($id);
         
        $thumbnail_name = null;
        if ($request->hasFile('thumbnail')) {
         //   $request->file('thumbnail')->store('/'); //store to storage link
            $thumbnail_name = $request->thumbnail->hashName(); // get name only
             $path = Storage::putFile('/',
             $request->file('thumbnail'));
        }
        $sliders = DB::table('sliders')
            ->where('id', $id)
            ->update(
                [
                    'title'   => $request->title,
                    'content' => $request->content,
                    'img'     => $thumbnail_name ?? $slider->img
                ]
            );
             Alert::success('Sukses', 'berhasil memperbarui slider');
              return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sliders =  DB::table('sliders')->where('id', $id)->delete();
        return redirect()->route('admin.slider.index');
    }

    public function sliderAPI()
    {
        return response()->json([
            'succes' => true,
            'data' =>  Sliders::all()
        ]);
    }
}
