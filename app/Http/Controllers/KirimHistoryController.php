<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class KirimHistoryController extends Controller
{
    public function index(Request $request)
    {

        $kirim_history = DB::table('kirim_history')->orderBy('kirim_history.created_at', 'desc')->get();
        if ($request->ajax()) {
            return datatables()->of($kirim_history)
                ->addIndexColumn()

                ->make(true);
        }
        return view('admin.kirim.index', compact('kirim_history'));
    }
}
