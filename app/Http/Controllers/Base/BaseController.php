<?php

namespace App\Http\Controllers\Base;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller {
    public function jsonSuccessResponse($data, $message = "ok"){
        return response()->json([
            'succes' => true,
            'message' => $message ,
            'data' => $data
        ]);
    }

    public function jsonFailResponse($data, $message = "ok"){
        return response()->json([
            'succes' => false,
            'message' => $message ,
            'data' => $data
        ]);
    }
}
