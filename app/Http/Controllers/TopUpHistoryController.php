<?php

namespace App\Http\Controllers;

use App\top_up_history;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Alert;

class TopUpHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $topup = DB::table('top_up_history')
            ->leftJoin('users', 'users.id', '=', 'top_up_history.user_id')
            ->where('top_up_history.status', '!=', 0)
            ->orderBy('top_up_history.updated_at', 'desc')
            ->select('top_up_history.*','top_up_history.created_at as created_at','users.name', 'users.no_hp' )
            ->get();
            
         

        if ($request->ajax()) {
            return datatables()->of($topup)
                ->addColumn('action', 'admin.topup.action')
                ->editColumn('status', function ($row) {
                    if ($row->status == 1) {
                        return '<h6 class="badge badge-info">sudah</h6>';
                    }
                    if ($row->status == 2) {
                        return '<h6 class="badge badge-secondary">dibatalkan</h6>';
                    }
                })
                ->rawColumns(['action', 'status'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.topup.index', compact('topup'));
    }

    public function topupMasuk(Request $request)
    {
     
        $topup = DB::table('top_up_history')
            ->leftJoin('users', 'users.id', '=', 'top_up_history.user_id')
            ->where('top_up_history.status', '=', 0)
           ->select('top_up_history.*','top_up_history.created_at as created_at','users.name', 'users.no_hp' )
            ->orderBy('top_up_history.created_at', 'desc');
            
   

        if ($request->ajax()) {
            
            return datatables()->of($topup->get())
                ->addColumn('action', 'admin.topup.action')
                ->editColumn('status', function ($row) {
                    return ($row->status != 0) ? '<h6 class="badge badge-info">sudah</h6>' : '<h6 class="badge badge-danger">belum</h6>';
                })
                ->rawColumns(['action', 'status'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.topup.index_topup_masuk', compact('topup'));
    }

    public function verifikasiTopupMasuk(Request $request, $id)
    {

        try {
            
            DB::beginTransaction();
            
            DB::table('top_up_history')
                ->where('kode_unik', $id)
                ->update(
                    [
                        'status'   => 1,
                        'verifikator' => Auth::user()->name ??  Auth::user()->username,
                        'updated_at' => \Carbon\Carbon::now()
                    ]
                );


            $user_saldo = DB::table('users')
                ->where('no_hp', '=', $request->no_hp)->first()->saldo;


            $user_token_fcm = DB::table('users')
                ->where('no_hp', '=', $request->no_hp)->first()->token_fcm;

            DB::table('users')
                ->where('no_hp', $request->no_hp)
                ->update(
                    [
                        'saldo'   => $user_saldo + $request->kepeng,
                    ]
                );
            DB::commit();
            Alert::success('Sukses', 'Berhasil verifikasi top up');


            $msg = array(
                'body'  => 'Top Up anda dengan kode ' . $request->kode . ' telah diverifikasi oleh admin, silahkan cek saldo anda',
                'title'    => 'Kepeng'
            );

            $fields = array(
                'to' => $user_token_fcm,
                'notification'    => $msg
            );
            $headers = array(
                'Authorization: key=AAAA6priZMM:APA91bE5kIppJ2Rh3bGfAoKiNJOoB1JGqBZTLJT90u900xrAp2xsnXdmfO5vh7rpdPyBp6aAEhS2QsJx-xiCDSNl6aDkqllBv7LtB7XgKJgkPmEb4Tpm7TosKOtnUVAMxim8QUIl-7XV',
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            return redirect()->back();
        } catch (\Throwable $th) {
              DB::rollback();
            throw $th;
        }
    }

    public function rejectTopupMasuk(Request $request, $id)
    {

        try {
         
            
            DB::table('top_up_history')
                ->where('kode_unik', $id)
                ->update(
                    [
                        'status'   => 2,
                        'verifikator' => Auth::user()->name ??  Auth::user()->username,
                        'updated_at' => \Carbon\Carbon::now()
                    ]
                );


            $user_token_fcm = DB::table('users')
                ->where('no_hp', '=', $request->no_hp)->first()->token_fcm;
                
            
                
              

            Alert::success('Sukses', 'Top up berhasil di Batalkan');


             $msg = array(
                'body'  => 'Top Up anda dengan kode ' . $request->kode . ' telah dibatalkan oleh admin',
                'title'    => 'Kepeng'
            );

            $fields = array(
                'to' => $user_token_fcm,
                'notification'    => $msg
            );
            $headers = array(
                'Authorization: key=AAAA6priZMM:APA91bE5kIppJ2Rh3bGfAoKiNJOoB1JGqBZTLJT90u900xrAp2xsnXdmfO5vh7rpdPyBp6aAEhS2QsJx-xiCDSNl6aDkqllBv7LtB7XgKJgkPmEb4Tpm7TosKOtnUVAMxim8QUIl-7XV',
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            return redirect()->back();
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function topupManual()
    {
        $users = DB::table('users')
            ->where('hak_akses_id', '=', 3)->get();
        return view('admin.topup.top_up_manual', compact('users'));
    }

    public function tambahPoint(Request $request)
    {

        $array =  $request->no_hp;
        $array = explode(",", $array);

        $users_point = DB::table('users')->where('no_hp', '=', $array[0])->first()->poin;


        $text_aksi = '';
        if ($request->aksi == 'penambahan') {
            $update_point = $request->point + $users_point;
            $text_aksi = 'penambahan';
        } else if ($request->aksi == 'pengurangan') {
            $update_point = $users_point -  $request->point;
            $text_aksi = 'pengurangan';
        }

        try {

            DB::table('users')
                ->where('no_hp', $array[0])
                ->update(
                    [
                        'poin'   => $update_point,
                    ]
                );

            $user_token_fcm = DB::table('users')
                ->where('no_hp', '=',  $array[0])->first()->token_fcm;

            DB::table('top_up_admin_history')->insert(
                [
                    'id_admin'   => Auth::user()->id,
                    'nama_admin' => Auth::user()->name,
                    'id_user'    => $array[1],
                    'nama_user'  => $array[2],
                    'no_hp_user' => $array[0],
                    'poin'      => $request->point,
                    'aksi'       => $request->aksi,
                    'kepeng'     => 0,
                    'created_at' => \Carbon\Carbon::now()
                ]
            );
            
            Alert::success('Sukses', 'Proses berhasil');


            $msg = array(
                'body'  => 'Anda mendapat ' . $text_aksi . ' point sebesar ' . $request->point . ' dari ' . Auth::user()->username,
                'title'    => 'Kepeng'
            );

            $fields = array(
                'to' => $user_token_fcm,
                'notification'    => $msg
            );
            $headers = array(
                'Authorization: key=AAAA6priZMM:APA91bE5kIppJ2Rh3bGfAoKiNJOoB1JGqBZTLJT90u900xrAp2xsnXdmfO5vh7rpdPyBp6aAEhS2QsJx-xiCDSNl6aDkqllBv7LtB7XgKJgkPmEb4Tpm7TosKOtnUVAMxim8QUIl-7XV',
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);


            return redirect()->back();
        } catch (\Throwable $th) { }
    }




    public function tambahPointIndex(Request $request)
    {
        $users = DB::table('users')->where('hak_akses_id', '=', 3)->get();
        return view('admin.topup.top_up_point', compact('users'));
    }


    public function tambahkepeng(Request $request)
    {


        DB::beginTransaction();
        $array =  $request->no_hp;
        $array = explode(",", $array);

        $user_saldo = DB::table('users')->where('no_hp', '=', $array[0])->first()->saldo;

        $text_aksi = '';
        if ($request->aksi == 'penambahan') {
            $update_saldo = $request->kepeng + $user_saldo;
            $text_aksi = 'penambahan';
        } else if ($request->aksi == 'pengurangan') {
            $update_saldo = $user_saldo -  $request->kepeng;
            $text_aksi = 'pengurangan';
        }

        try {

            DB::table('users')
                ->where('no_hp', $array[0])
                ->update(
                    [
                        'saldo'   => $update_saldo,
                    ]
                );


            $user_token_fcm = DB::table('users')
                ->where('no_hp', '=',  $array[0])->first()->token_fcm;


            DB::table('top_up_admin_history')->insert(
                [
                    'id_admin'   => Auth::user()->id,
                    'nama_admin' => Auth::user()->name,
                    'id_user'    => $array[1],
                    'nama_user'  => $array[2],
                    'no_hp_user' => $array[0],
                    'poin'      => 0,
                    'aksi'       => $request->aksi,
                    'saldo_user' => $user_saldo,
                    'saldo_user_setelah' => $update_saldo,
                    'kepeng'     => $request->kepeng,
                    'created_at' => \Carbon\Carbon::now()
                ]
            );
            DB::commit();
            Alert::success('Sukses', 'berhasil');



            $msg = array(
                'body'  => 'Anda mendapat ' . $text_aksi . ' kepeng sebesar Rp.' . number_format($request->kepeng, 0, ',', '.') . ' dari ' . Auth::user()->username,
                'title'    => 'Kepeng'
            );

            $fields = array(
                'to' => $user_token_fcm,
                'notification'    => $msg
            );
            $headers = array(
                'Authorization: key=AAAA6priZMM:APA91bE5kIppJ2Rh3bGfAoKiNJOoB1JGqBZTLJT90u900xrAp2xsnXdmfO5vh7rpdPyBp6aAEhS2QsJx-xiCDSNl6aDkqllBv7LtB7XgKJgkPmEb4Tpm7TosKOtnUVAMxim8QUIl-7XV',
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);


            return redirect()->back();
        } catch (\Throwable $th) {
              DB::rollback();
            throw $th;
        }
    }

    public function adminHistory(Request $request)
    {

        $history = DB::table('top_up_admin_history')->orderBy('top_up_admin_history.created_at', 'desc')->get();

        if ($request->ajax()) {
            return datatables()->of($history)
                ->editColumn('aksi', function ($row) {
                    return ($row->aksi != "pengurangan") ? '<h6 class="badge badge-success">Penambahan</h6>' : '<h6 class="badge badge-danger">Pengurangan</h6>';
                })
                ->rawColumns(['aksi'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.admin_history.index', compact('history'));
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
