<?php

namespace App\Http\Controllers\API\Publik;

use App\Http\Controllers\Controller;
use App\Helpers\Constants;
use Illuminate\Http\Request;
use App\Services\KepengService;
use App\Services\TransactionService;
use App\Models\XenQR;
use App\Models\XenFixedVA;
use stdClass;
use App\User;
use chillerlan\QRCode\QRCode;
use App\Services\Transaction\PayCallbackTransact;

class TransactionController extends Controller
{
    // transaction service instance
    public $transSvc;

    public function __construct()
    {
        // $this->that
        $this->transSvc = new TransactionService();
    }

    public function getPhpInput($jsonDecoded = true){
        $phpi = file_get_contents('php://input');
        return $jsonDecoded ? json_decode($phpi) : $phpi;
    }

    public function doProccess($type, $data){
        $msg = "";
        $code = 200;
        $res = null;
        try{
            $proc = $this->transSvc
                ->setData($data)->setType($type)
                ->proccess();
            $ret = $proc;
            $res = null;
            if($ret->result){
                $res = $ret->result->getResult();
            }
            $code = $proc->code;
            $msg = $proc->message;
        }catch(\Exception $e){
            $msg = $e->getMessage();
            $code = 500;
        }
        
        return jsonResponse($code, $msg, $res);
    }

    public function topup(){
        // handle by TopUpController@store
    }

    public function topupCallback(){
        return $this->doProccess(Constants::TRANS_PAY_CALLBACK, null);
    }

    public function payout(Request $req){
        // to do : set user by jwt
        $user = User::where('id','>',1)->first();
        $data = [
            "amount" => $req->amount,
            "user" => $user
        ];
        return $this->doProccess(Constants::TRANS_PAYOUT, $data);
    }

    public function payoutCallback(){
        //code
        return $this->doProccess(Constants::TRANS_PAYOUT_CALLBACK, null);
    }

    public function qrcodePay(){
        //code
        echo __FUNCTION__;
    }

    public function qrcodeCallback(){
        //code
        echo __FUNCTION__;
        $data = [ "data" ];
        return $this->doProccess(Constants::TRANS_QRCODE_CALLBACK, $data);
    }

    public function fixedvaCreatedCallback(Request $req){
        try{
            $json = $this->getPhpInput();
            // toFile($json, "dummyFixedvaCreatedCallback");

            $fva = XenFixedVA::where("account_number", $json->account_number);
            $fva->update([ "status" => $json->status ]);

            return response()->json($json);
        }catch(\Exception $e){
            return $e->getMessage();
        }
    }

    public function fixedvaPaidCallback(Request $req){
        try{
            $json = $this->getPhpInput();
            // toFile(presonRet($json), "dummyFvaPaidcallJson"); //.date("YmdHis")

            $fva = XenFixedVA::where("account_number", $json->account_number)->first();
            if($fva){
                $user = $fva->user;
            }
            
            $pcall = new PayCallbackTransact();
            $ret = $pcall->setUseFVA(true)->proccess();

            //update topup_history
            // toFile(presonRet($ret), "dummyFvaPaidcallRet"); //.date("YmdHis")

            return response()->json($json);
        }catch(\Exception $e){
            return response()->json($e->getMessage())->setStatusCode(500);;
        }
    }
}
