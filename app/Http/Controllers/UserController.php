<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Kirim_history;
use Validator;
use Auth;
use Illuminate\Support\Facades\Hash;
use Alert;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $users = User::all();

        // if ($request->ajax()) {
        //     return datatables()->of($users)
        //         ->addColumn('action', 'admin.post.action')
        //         ->addIndexColumn()
        //         ->make(true);
        // }
        // return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user =  DB::table('users')->where('id', $id)->delete();
        Alert::success('Sukses', 'User berhasil dihapus');
        return redirect()->back();
    }

    public function pinCreate(Request $request){

        $validator = Validator::make($request->all(),  [
            'pin' => 'required|digits_between:6,6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'succes' => false,
                'error' => '',
                'message' => $validator->errors()->first()
            ]);
        }

        $user = Auth::user();
        $user->pin_withdraw = Hash::make($request->pin);
        $user->save();

        return response()->json([
            'succes' => true,
            'error' => '',
            'message' => 'PIN berhasil di buat'
        ]);
    }

    public function pinCheck(Request $request){

        $validator = Validator::make($request->all(),  [
            'pin' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'succes' => false,
                'error' => '',
                'message' => $validator->errors()->first()
            ]);
        }

        $user_pin = Auth::user()->pin_withdraw;

        $validated = $validator->validate();
        $status = Hash::check($validated['pin'], $user_pin);

        if ($status) {
            return response()->json([
                'succes' => true,
                'error' => '',
                'message' => 'PIN benar'
            ]);
        } else {
            return response()->json([
                'succes' => false,
                'error' => '',
                'message' => 'PIN salah silahkan coba lagi'
            ]);
        }
    }


    public function checkPin(Request $request)
    {
        $pin_kirim =  $request->pin;
        $user_pin = DB::table('users')
            ->where('users.id', '=', Auth::user()->id)
            ->first()->pin;

        $status = Hash::check($pin_kirim, $user_pin);

        if ($status) {
            return response()->json([
                'succes' => true,
                'error' => '',
                'message' => 'PIN benar'
            ]);
        } else {
            return response()->json([
                'succes' => false,
                'error' => '',
                'message' => 'PIN salah silahkan coba lagi'
            ]);
        }
    }


    public function pengguna(Request $request)
    {
        // $users = User::all();

        $users = DB::table('users')
            ->leftjoin('hak_akses', 'users.hak_akses_id', '=', 'hak_akses.id')
            ->where('users.hak_akses_id', '=', '3')
            ->select('users.*', 'hak_akses.tipe')
            ->orderBy('created_at', 'desc')
            ->get();

        if ($request->ajax()) {
            return datatables()->of($users)
                ->addColumn('action', 'admin.user.action_pengguna')
                ->editColumn('active', function ($row) {
                    if ($row->active == 1) {
                        return '<h6 class="badge badge-success">aktif</h6>';
                    }
                    if ($row->active == 0) {
                        return '<h6 class="badge badge-danger">suspend</h6>';
                    }
                })
                ->editColumn('action', function ($row) {
                    if ($row->active == 1) {
                        return '<a href="#" id={{ $id }} style="margin-bottom: 10px" class="btn btn-primary btn-sm disable" role="button"
                        aria-pressed="true" data-toggle="modal" data-target="#modalDelete"><i class="fas fa-user-slash"></i></i> Disable&nbsp</a>
                        
                    <a href="#" id={{ $id }} style="margin-bottom: 10px" class="btn btn-danger btn-sm reset" role="button"
                        aria-pressed="true" data-toggle="modal" data-target="#modalDelete"><i class="fas fa-edit"></i></i> </a>

                    <a href="#" id={{ $id }} style="margin-bottom: 10px" class="btn btn-info btn-sm resetpin" role="button"
                        aria-pressed="true" data-toggle="modal" data-target="#modalDelete"><i class="fas fa-key"></i></i> </a>
                        
                        ';
                    }
                    if ($row->active == 0) {
                        return '<a href="#" id={{ $id }} style="margin-bottom: 10px" class="btn btn-success btn-sm enable" role="button"
                        aria-pressed="true" data-toggle="modal" data-target="#modalDelete"><i class="fas fa-user-check"></i></i>&nbsp&nbspEnable&nbsp   </a>
                        
                           <a href="#" id={{ $id }} style="margin-bottom: 10px" class="btn btn-danger btn-sm reset" role="button"
                        aria-pressed="true" data-toggle="modal" data-target="#modalDelete"><i class="fas fa-edit"></i></i> </a>

                            <a href="#" id={{ $id }} style="margin-bottom: 10px" class="btn btn-info btn-sm resetpin" role="button"
                        aria-pressed="true"><i class="fas fa-key"></i></i> </a>
                        ';
                    }
                })
                ->rawColumns(['action', 'active'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.user.index', compact('users'));
    }
    
    
    
    public function resetPassUser(Request $request, $id){
        
      
         DB::table('users')
            ->where('no_hp', $request->no_hp3)
            ->update(
                [
                    'pin'   => Hash::make($request->password),
                    'updated_at' => \Carbon\Carbon::now()
                ]
            );
        Alert::success('Sukses', 'Password User Berhasil Direset');

        return redirect()->back();
    }

    public function resetPinUser(Request $request, $id){

        $validator = Validator::make($request->all(),  [
            'pin' => 'required|numeric|digits:6',
            'nohp' => 'required'
        ]);

        if ($validator->fails()) {

            Alert::error('Error', $validator->errors()->first());
            return redirect()->back();
        }

        DB::table('users')
            ->where('no_hp', $request->nohp)
            ->update(
                [
                    'pin_withdraw'   => Hash::make($request->pin),
                    'updated_at' => \Carbon\Carbon::now()
                ]
            );
        Alert::success('Sukses', 'PIN User Berhasil Direset');

        return redirect()->back();
    }


    public function statusUSer(Request $request, $id)
    {

        $status = '';
        $message = '';
        if ($request->status == 'disable') {
            $status = 0;
            $message = 'berhasil mendisable user';
        }

        if ($request->status == 'enable') {
            $status = 1;
            $message = 'berhasil mengaktifkan user';
        }

        DB::table('users')
            ->where('no_hp', $request->no_hp)
            ->update(
                [
                    'active'   => $status,
                    'updated_at' => \Carbon\Carbon::now()
                ]
            );



        Alert::success('Sukses', $message);
        return redirect()->back();
    }


    public function admin(Request $request)
    {
        // $users = User::all();

        $users = DB::table('users')
            ->leftjoin('hak_akses', 'users.hak_akses_id', '=', 'hak_akses.id')
            ->whereIn('users.hak_akses_id', [1, 2])
            ->select('users.*', 'hak_akses.tipe')
            ->orderBy('created_at', 'desc')
            ->get();

        if ($request->ajax()) {
            return datatables()->of($users)
                ->addColumn('action', 'admin.user.action')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('admin.user.index_admin', compact('users'));
    }



    public function adminCreate()
    {
        return view('admin.user.create');
    }


    public function adminEdit($id)
    {
        $users = DB::table('users')->where('id', '=', $id)->first();

        return view('admin.user.edit', compact('users'));
    }
    
     public function adminEditList($id)
    {
        $users = DB::table('users')->where('id', '=', $id)->first();

        return view('admin.user.edit_list', compact('users'));
    }
    
    
    public function adminUpdate(Request $request)
    {
       
      
      
        
         DB::table('users')
            ->where('id',Auth::user()->id)
            ->update(
                [
                    'pin'   => Hash::make($request->password),
                    'updated_at' => \Carbon\Carbon::now()
                ]
            );
        Alert::success('Sukses', 'Password Berhasil Diubah');

        return redirect()->back();
    }



 public function adminUpdateList(Request $request)
    {
       
         
       if($request->password==null){
             $pass = DB::table('users')->select('pin')->where('id', '=', $request->id)->first();
        
             DB::table('users')
            ->where('id',$request->id)
            ->update(
                [
                    'hak_akses_id'   => $request->aksi,
                    'pin'   => $pass->pin,
                    'updated_at' => \Carbon\Carbon::now()
                ]
            );
       }
       else{
             DB::table('users')
            ->where('id',$request->id)
            ->update(
                [
                    'hak_akses_id'   => $request->aksi,
                    'pin'   => Hash::make($request->password),
                    'updated_at' => \Carbon\Carbon::now()
                ]
            );
       }
    

       
        Alert::success('Sukses', 'Data Berhasil Diubah');

        return redirect()->back();
    }


    public function adminStore(Request $request)
    {
        // dd($request->all());
        // return view('admin.user.edit');
        DB::table('users')->insert(
            [
                'username'          =>    $request->username,
                'pin'               =>     Hash::make($request->password),
                'hak_akses_id'      =>   $request->hak_akses,
                'created_at'        => \Carbon\Carbon::now(),
            ]
        );
        Alert::success('Sukses', 'User berhasil ditambahkan');

        return redirect()->back();
    }




    public function UserDetails(Request $request)
    {
        $user = $request->user();
        $user->xenFixedVa;

        return response()->json($user);
    }

    public function KirimUang(Request $request)
    {


        $validator = Validator::make($request->all(),  [
            'no_hp' => 'required|max:20|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'saldo_transfer' => 'required|max:100',
        ]);


        if ($validator->fails()) {
            return response()->json([
                'succes' => false,
                'error' => '',
                'message' => $validator->errors()->first()
            ]);
        }

        if (User::where('no_hp', $request->no_hp)->get()->isEmpty()) {
            return response()->json([
                'succes' => false,
                'error' => 'no_hp_1',
                'message' => 'no hp tidak terdaftar'
            ]);
        } else {

            //check saldo cukup atau tidak
            if (Auth::user()->saldo < $request->saldo_transfer || $request->saldo_transfer < 0) {
                return response()->json([
                    'succes' => false,
                    'error' => 'saldo_tidak_cukup',
                    'message' => 'Maaf saldo anda tidak cukup'
                ]);
            } else {
                if (Auth::user()->no_hp == $request->no_hp) {
                    return response()->json([
                        'succes' => false,
                        'error' => 'no_hp_2',
                        'message' => 'Nomor yang anda masukan tidak tepat'
                    ]);
                } else {

                    if (($request->aksi) == 'konfirmasi') {

                        if (User::where('no_hp', $request->no_hp)->get()->isEmpty()) {
                            return response()->json([
                                'succes' => false,
                                'error' => 'no_hp_2',
                                'message' => 'Nomor yang anda masukan tidak tepat'
                            ]);
                        } else {

                            return response()->json([
                                'succes' => true,
                                'error' => '',
                                'message' =>  User::where('no_hp', $request->no_hp)->first()->name

                            ]);
                        }
                    }
                    if (($request->aksi) == 'kirim') {
                        $invID = '';
                        try {
                            // $transaction_fee = DB::table('transaction_fee')->select('transaction_fee')->first()->transaction_fee;
                            // $kepeng_fee = $request->saldo_transfer + $transaction_fee;
                            $kepeng = $request->saldo_transfer;


                            if (Auth::user()->saldo >= $kepeng) {


                                DB::beginTransaction();
                                // $transaction_fee = DB::table('transaction_fee')->select('transaction_fee')->first()->transaction_fee;
                                // $trading_code = DB::table('kirim_history')->select('trading_code')->latest()->first()->trading_code;

                                $kode_top_up = 0;

                                $check_data = DB::table('kirim_history')->get();
                                if ($check_data->isEmpty()) {
                                    $kode_top_up = 0;
                                } else {
                                    $kode_top_up = DB::table('kirim_history')->select('trading_code')->latest()->first()->trading_code;
                                }

                                $int = (int) $kode_top_up + 1;
                                $int =  (string) $int;
                                $invID = str_pad($int, 3, '0', STR_PAD_LEFT);



                                //kurangi saldo pengirim
                                $user_pengirim = Auth::user();
                                $user_pengirim->saldo = $user_pengirim->saldo - ($request->saldo_transfer);
                                $user_pengirim->save();

                                //tambah saldo penerima
                                $user_penerima =  User::where('no_hp', $request->no_hp)->first();
                                $user_penerima->saldo = $user_penerima->saldo + $request->saldo_transfer;
                                $user_penerima->save();

                                $user_token_fcm = DB::table('users')
                                    ->where('no_hp', '=', $request->no_hp)->first()->token_fcm;



                                DB::table('kirim_history')->insert(
                                    [
                                        'trading_code'     => $invID,
                                        'id_pengirim'     => Auth::user()->id,
                                        'id_penerima'     => $user_penerima->id,
                                        'penerima'        => $user_penerima->name,
                                        'pengirim'        => Auth::user()->name,
                                        'pesan'           => $request->pesan,
                                        'no_hp'           => $request->no_hp,
                                        'kepeng'          => $request->saldo_transfer,
                                        
                                        'transaction_fee' => 0,
                                        'total'           => $request->saldo_transfer,
                                        'created_at'      => \Carbon\Carbon::now(),
                                    ]
                                );
                                
                                
                                
                                

                                DB::commit();

                                $msg = array(
                                    'body'  => 'Anda mendapat transfer kepeng sebesar Rp. ' . number_format($request->saldo_transfer, 0, ',', '.') . ', dari ' . Auth::user()->name . ' (' . Auth::user()->no_hp . ')',
                                    'title'    => 'Kepeng'
                                );

                                $fields = array(
                                    'to' => $user_token_fcm,
                                    'notification'    => $msg
                                );
                                $headers = array(
                                    'Authorization: key=AAAA6priZMM:APA91bE5kIppJ2Rh3bGfAoKiNJOoB1JGqBZTLJT90u900xrAp2xsnXdmfO5vh7rpdPyBp6aAEhS2QsJx-xiCDSNl6aDkqllBv7LtB7XgKJgkPmEb4Tpm7TosKOtnUVAMxim8QUIl-7XV',
                                    'Content-Type: application/json'
                                );
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                                $result = curl_exec($ch);
                                curl_close($ch);

                                return response()->json([
                                    'succes' => true,
                                    'error' => '',
                                    'message' =>  $invID
                                ]);
                            } else {
                                return response()->json([
                                    'succes' => false,
                                    'error' => 'saldo_tidak_cukup',
                                    'message' => 'Maaf saldo anda tidak cukup'
                                ]);
                            }
                        } catch (\Throwable $th) {

                            return response()->json([
                                'succes' => false,
                                'error' => '',
                                'message' => 'gagal terjadi kesalahan' + $th,

                            ]);
                        }
                    }
                }
            }
        }
    }
    
    
    
    public function KirimPoin(Request $request)
    {


        $validator = Validator::make($request->all(),  [
            'no_hp' => 'required|max:20|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'poin' => 'required|max:100',
        ]);


        if ($validator->fails()) {
            return response()->json([
                'succes' => false,
                'error' => '',
                'message' => $validator->errors()->first()
            ]);
        }

        if (User::where('no_hp', $request->no_hp)->get()->isEmpty()) {
            return response()->json([
                'succes' => false,
                'error' => 'no_hp_1',
                'message' => 'no hp tidak terdaftar'
            ]);
        } else {

            //check saldo cukup atau tidak
            if (Auth::user()->poin < $request->poin || $request->poin < 0) {
                return response()->json([
                    'succes' => false,
                    'error' => 'poin_tidak_cukup',
                    'message' => 'Maaf poin anda tidak cukup'
                ]);
            } else {
                if (Auth::user()->no_hp == $request->no_hp) {
                    return response()->json([
                        'succes' => false,
                        'error' => 'no_hp_2',
                        'message' => 'Nomor yang anda masukan tidak tepat'
                    ]);
                } else {

                    if (($request->aksi) == 'konfirmasi') {

                        if (User::where('no_hp', $request->no_hp)->get()->isEmpty()) {
                            return response()->json([
                                'succes' => false,
                                'error' => 'no_hp_2',
                                'message' => 'Nomor yang anda masukan tidak tepat'
                            ]);
                        } else {

                            return response()->json([
                                'succes' => true,
                                'error' => '',
                                'message' =>  User::where('no_hp', $request->no_hp)->first()->name

                            ]);
                        }
                    }
                    if (($request->aksi) == 'kirim') {
                        $invID = '';
                        try {
                            // $transaction_fee = DB::table('transaction_fee')->select('transaction_fee')->first()->transaction_fee;
                            // $kepeng_fee = $request->saldo_transfer + $transaction_fee;
                            $poin = $request->poin;


                            if (Auth::user()->poin >= $poin) {


                                DB::beginTransaction();
                                // $transaction_fee = DB::table('transaction_fee')->select('transaction_fee')->first()->transaction_fee;
                                // $trading_code = DB::table('kirim_history')->select('trading_code')->latest()->first()->trading_code;

                                $kode_top_up = 0;

                                $check_data = DB::table('kirim_history')->get();
                                if ($check_data->isEmpty()) {
                                    $kode_top_up = 0;
                                } else {
                                    $kode_top_up = DB::table('kirim_history')->select('trading_code')->latest()->first()->trading_code;
                                }

                                $int = (int) $kode_top_up + 1;
                                $int =  (string) $int;
                                $invID = str_pad($int, 3, '0', STR_PAD_LEFT);



                                //kurangi saldo pengirim
                                $user_pengirim = Auth::user();
                                $user_pengirim->poin = $user_pengirim->poin - ($request->poin);
                                $user_pengirim->save();

                                //tambah saldo penerima
                                $user_penerima =  User::where('no_hp', $request->no_hp)->first();
                                $user_penerima->poin = $user_penerima->poin + $request->poin;
                                $user_penerima->save();

                                $user_token_fcm = DB::table('users')
                                    ->where('no_hp', '=', $request->no_hp)->first()->token_fcm;



                                DB::table('kirim_history')->insert(
                                    [
                                        'trading_code'     => $invID,
                                        'id_pengirim'     => Auth::user()->id,
                                        'id_penerima'     => $user_penerima->id,
                                        'penerima'        => $user_penerima->name,
                                        'pengirim'        => Auth::user()->name,
                                        'pesan'           => $request->pesan,
                                        'no_hp'           => $request->no_hp,
                                        'poin'          => $request->poin,
                                        'transaction_fee' => 0,
                                        'total'           => $request->saldo_transfer,
                                        'created_at'      => \Carbon\Carbon::now(),
                                    ]
                                );
                                
                                
                                
                                

                                DB::commit();

                                $msg = array(
                                    'body'  => 'Anda mendapat transfer poin sebesar  ' . $request->poin . ', dari ' . Auth::user()->name . ' (' . Auth::user()->no_hp . ')',
                                    'title'    => 'Kepeng'
                                );

                                $fields = array(
                                    'to' => $user_token_fcm,
                                    'notification'    => $msg
                                );
                                $headers = array(
                                    'Authorization: key=AAAA6priZMM:APA91bE5kIppJ2Rh3bGfAoKiNJOoB1JGqBZTLJT90u900xrAp2xsnXdmfO5vh7rpdPyBp6aAEhS2QsJx-xiCDSNl6aDkqllBv7LtB7XgKJgkPmEb4Tpm7TosKOtnUVAMxim8QUIl-7XV',
                                    'Content-Type: application/json'
                                );
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                                curl_setopt($ch, CURLOPT_POST, true);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                                $result = curl_exec($ch);
                                curl_close($ch);

                                return response()->json([
                                    'succes' => true,
                                    'error' => '',
                                    'message' =>  $invID
                                ]);
                            } else {
                                return response()->json([
                                    'succes' => false,
                                    'error' => 'saldo_tidak_cukup',
                                    'message' => 'Maaf poin anda tidak cukup'
                                ]);
                            }
                        } catch (\Throwable $th) {

                            return response()->json([
                                'succes' => false,
                                'error' => '',
                                'message' => 'gagal terjadi kesalahan' + $th,

                            ]);
                        }
                    }
                }
            }
        }
    }
    
    
    
    
}
