<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use App\Models\XenFixedVA;
use App\Models\Topup;

class User extends Authenticatable
{
    use Notifiable, HasRoles;
    use HasApiTokens; // laravel auth

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'password',
        'no_hp',
        'pin',
        'username',
        'hak_akses_id',
        'tmpt_lahir',
        'tgl_lahir',
        'lokasi',
        'token_otp',
        'created_at',
    ];

    protected $appends = ['transaction_fee'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function xenFixedVa()
    {
        return $this->hasMany(XenFixedVA::class);
    }

    public function fixedVas()
    {
        $data = [
            "mandiri" => $this->xenFixedVa->where("bank_code", "MANDIRI")->first(),
            "bni" => $this->xenFixedVa->where("bank_code", "BNI")->first(),
            "bri" => $this->xenFixedVa->where("bank_code", "BRI")->first(),
        ];
        return $data;
    }

    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    public function addSaldo($amt = 0){
        return $this->update(['saldo' => $this->saldo + abs($amt)]);
    }

    public function topups()
    {
        return $this->hasMany(Topup::class);
    }


    public function getTransactionfeeAttribute()
    {
        return    DB::table('transaction_fee')->select('transaction_fee')->first()->transaction_fee;
    }

    public function getAuthPassword()
    {
        return $this->pin;
    }
}
