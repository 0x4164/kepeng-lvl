<?php

namespace App;

use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Sliders extends Model
{
    public function getThumbnail()
    {
        // jika mempunyai value dari database
        if ($this->img) {
            return asset('public/storage/' . $this->img);
        }

        // jika kedua kondisi diatas tidak terpenuhi 
        return 'https://via.placeholder.com/150x200.png?text=No+Cover';
    }
}
