<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pencairan extends Model
{
    protected $table = 'pencairan';
    const UPDATED_AT = null;
   
    protected $guarded = [];

    public function getCreatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['created_at'])
            ->format('d-m-Y H:i');
    }

    public function getUpdatedAtAttribute()
    {
        if($this->attributes['updated_at']==null){

            return "-"; 
        }
        else{
            $tgl = \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->format('d-m-Y H:i');
           return $tgl ;
        }
     
    }

    // public function getStatusAttribute()
    // {
        
    //     if($this->attributes['status']==1){
    //         return "disetujui";
    //     }
    //     else if($this->attributes['status']==2){
    //         return "dibatalkan";
    //     }
    //     else{
    //         return "diproses";
    //     }   
    // }

}