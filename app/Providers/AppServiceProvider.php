<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

use Carbon\Carbon;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {}
         // For example im gonna locale all dates to Indonesian (ID)
   

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::include('admin.includes.include', 'textinput');
        // Blade::include('components.modal', 'modal');  
        // Blade::component('components.modal', 'modal'); 
        Blade::component('components.modal', 'modal');

        Blade::component('components.alert', 'alert');

        // Blade::include('admin.category.create', 'create');
        Blade::include('admin.category.createOrUpdate', 'createOrUpdate');
        Blade::include('admin.category.delete', 'delete');
        Blade::include('admin.category.data', 'data');

        Blade::include('components.table', 'table');
        
    
    config(['app.locale' => 'id']);
	Carbon::setLocale('id');
	date_default_timezone_set('Asia/Jakarta');
    }
}
