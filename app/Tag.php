<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = [];

    // 1 tag bisa mempunyai banyak post
    public function post()
    {
        return $this->belongsToMany(Post::class);
    }

    //accesor carbon untuk merubah created_at
    public function getCreatedAtAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])
            ->format('d, M Y H:i');
    }

    //accesor menghitung jumlah post yang dimiliki tiap tag (Menambah object pada model result)
    protected $appends = ['total_post'];
    public function getTotalPostAttribute()
    {

        return count(collect($this->post));
    }
}
