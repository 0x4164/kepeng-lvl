<?php

namespace App\Helpers;

class Constants
{
    const EXTERNAL_CONTENT_PRODUCT_DEFAULT = "";
    const EXTERNAL_TEMPLATE_URL = "https://demo.getstisla.com/";

    const ASSET_GREY = "assets/images/sq300x300.png";
    const ASSET_UPLOAD_IMAGES_DIR = "storage/uploads/images/";
    const ASSET_UPLOAD_IMAGES_BANNER_DIR = "storage/uploads/images/banner/";
    const ASSET_UPLOAD_IMAGES_KATEGORI_DIR = "storage/uploads/images/kategori/";
    const ASSET_UPLOAD_PIC_DIR = "storage/uploads/pic/";
    const ASSET_UPLOAD_DOC_DIR = "storage/uploads/documents/";
    const ASSET_UPLOAD_PIC_THUMB_DIR = "storage/uploads/thumbnails/";
    const TEMP_DIR = "app/tmp/";

    const THUMBNAIL_DIMEN_300 = 300;
    const THUMBNAIL_DIMEN_200 = 200;

    const API_UNAUTHENTICATED = 'Unauthenticated';
    const API_UNAUTHORIZED = 'Unauthorized';
    const API_FORBIDDEN = 'Forbidden';
    const API_LOGGED_IN = 'Logged in';
    const API_SUCCESS = 'Success';
    const API_EMPTY = 'Empty';

    const HTTP_OK = 200;

    const API_ADDED = 'Added';
    const API_UPDATED = 'Updated';
    const API_DELETED = 'Deleted';

    const USER_CUSTOMER = 'customer';
    const USER_MERCHANT = 'merchant';
    const USER_COURIER = 'courier';
    const USER_OPERATOR = 'operator';
    const USER_ADMIN = 'admin';
    const USER_USER = 'user';

    const COLORS = [
        "red" => "rgb(255, 99, 132)",
        "orange" => "rgb(255, 159, 64)",
        "yellow" => "rgb(255, 205, 86)",
        "green" => "rgb(75, 192, 192)",
        "blue" => "rgb(54, 162, 235)",
        "purple" => "rgb(153, 102, 255)",
        "grey" => "rgb(201, 203, 207)"
    ];

    const NOTIF_STANDARD = [
        'text' => '0'
    ];

    const USER_ACTIVATED = [
        'y',
        't'
    ];

    const GALLERY_MAIN = "main";

    const TRANS_CHECKOUT = "t-checkout";
    const TRANS_PAY_CALLBACK = "t-pay-callback";
    const TRANS_PAYOUT = "t-payout";
    const TRANS_PAYOUT_CALLBACK = "t-payout-callback";
    const TRANS_QRCODE_CREATE = "t-qrcode-create";
    const TRANS_QRCODE_STATUS = "t-qrcode-status";
    const TRANS_QRCODE_PAY = "t-qrcode-pay";
    const TRANS_QRCODE_CALLBACK = "t-qrcode-callback";
    const TRANS_CREATE_FVA = "t-create-fva";
    const TRANS_FVA_CALLBACK = "t-fva-callback";
    const TRANS_CANCEL_BY_PEOPLE = "t-cancel-bypeople";
    const TRANS_FINISHED = "t-finished";
    const TRANS_UNKNOWN = "t-unknown";
    const TRANS_UNKNOWN_MSG = "please specify trans type";

    const TRANS_FILTER_STATUS_CURRENT = "current";
    const TRANS_FILTER_STATUS_FINISH = "finish";
    const TRANS_FILTER_STATUS_CANCEL = "cancel";
    
    const KEPENG_STATUS_PENDING = 0;
    const KEPENG_STATUS_VERIFIED = 1;
    const KEPENG_STATUS_REJECT = 2;
    
    const TRANS_STATUS_PAID = "PAID";

    const LOG_TRANSACTION = "transaction";
    const LOG_TRANSACTION_FILE = "laravel-trans.log";

    // thx you http://stackoverflow.com/questions/18742998/ddg#18743012
    public static function months()
    {
        $start    = (new \DateTime('2020-01-01'))->modify('first day of this year');
        $end      = (new \DateTime('2020-12-31'))->modify('last day of this year');
        $interval = \DateInterval::createFromDateString('1 month');
        $period   = new \DatePeriod($start, $interval, $end);
        $months = [];
        foreach ($period as $dt) {
            // echo $dt->format("Y-m") . "<br>\n";
            $months[] = $dt->format("M");
        }
        return $months;
    }

    public static function logFmt($class, $msg, $serialize)
    {
        return $class . " " . $msg . " " . serialize($serialize);
    }
}
