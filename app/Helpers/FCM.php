<?php

namespace App\Helpers;

class FCM{

    protected $token;

    public function setToken($token){
        $this->token = $token;
    }

    public function send($pesan){
        $msg = array(
            'body'  => $pesan,
            'title'    => 'Kepeng'
        );

        $fields = array(
            'to' => $this->token,
            'notification'    => $msg
        );
        $headers = array(
            'Authorization: key=AAAA6priZMM:APA91bE5kIppJ2Rh3bGfAoKiNJOoB1JGqBZTLJT90u900xrAp2xsnXdmfO5vh7rpdPyBp6aAEhS2QsJx-xiCDSNl6aDkqllBv7LtB7XgKJgkPmEb4Tpm7TosKOtnUVAMxim8QUIl-7XV',
            'Content-Type: application/json'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
    }
}