<?php

namespace App\Helpers;

class Xendit{

    protected $apikey;
    protected $endpoint;

    public function __construct(){
        $this->apikey = ENV('XENDIT_SERVER_KEY');
        $this->endpoint = 'https://api.xendit.co';
    }

    public function send($endpoint, $params = []){
        $curl = curl_init();
        $headers = array();
        $headers[] = 'Content-Type: application/json';

        $end_point = $this->endpoint.$endpoint;

        $payload = json_encode($params);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_USERPWD, $this->apikey.":");
        curl_setopt($curl, CURLOPT_URL, $end_point);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);

        $responseObject = json_decode($response, true);
        return $responseObject;
    }

    public static function createInvoice($params, &$response = []){
        
        $xendit = new self;
        $response = $xendit->send('/v2/invoices', $params);

        return $response;
    }
}