<?php

namespace App\Helpers;

use App\Models\Invoices;
use App\Models\Payouts;

class Utils{
    public static function transactionToken($id){
        $token = 
        config('app.name')."-".
        config('app.env')."-".
        config('app.debug')."-".
        $id; // md5("token".$i);
        // preout($token);
        $token = md5($token);
        return $token;
    }

    // invoice code generator
    public static function invoiceCode($kode = null, $type = "INV"){
        // $seq = (int) $seq;
        $date = date('YmdHis');
        $code = [
            ($type === 'INV') ? 'INV' : 'PAY',
            $date,
            $kode
        ];

        $code = implode("/", $code);

        return $code;

        // /*
        $n = 0;
        while($n < 10){
            $c2 = [$code, $seq];
            $code = implode("/", $c2);
            $inv = Invoices::where('external_id', $code)->first();
            // $seq
            if($inv){
                $seq++;
            }else{
                return $code;
                break;
            }
            $n++;
        }
        // */

        return $code;
    }
}
