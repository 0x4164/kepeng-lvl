<?php

namespace App\Exceptions;

use BeyondCode\Vouchers\Models\Voucher;

class XenQRCodeCreateFail extends \Exception
{
    protected $message = 'XenQRCode Creation Fail.';

    protected $data;

    public static function create($data)
    {
        return new static($data);
    }

    public function __construct($msg, $data = "")
    {
        $this->data = $data;
        $this->message = $msg;
    }
}