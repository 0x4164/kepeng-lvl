<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
// use App\Models\Transaction;

class XenFixedVA extends Model
{
    protected $table = "users_fixed_va";

    protected $fillable = [
        "is_closed",
        "status",
        "currency",
        "owner_id",
        "external_id",
        "bank_code",
        "merchant_code",
        "name",
        "account_number",
        "is_single_use",
        "expiration_date",
        "xen_id",
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public static function fromXendit($data){
        return [
            "is_closed" => $data["is_closed"],
            "status" => $data["status"],
            "currency" => $data["currency"],
            "owner_id" => $data["owner_id"],
            "external_id" => $data["external_id"],
            "bank_code" => $data["bank_code"],
            "merchant_code" => $data["merchant_code"],
            "name" => $data["name"],
            "account_number" => $data["account_number"],
            "is_single_use" => $data["is_single_use"],
            "expiration_date" => $data["expiration_date"],
            "xen_id" => $data["id"],
        ];
    }
}
