<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Models\Transaction;
use App\Pencairan;
class Payouts extends Model
{
    protected $table="payouts";
    protected $fillable = [
        'id',
        'external_id',
        'amount',
        'merchant_name',
        'status',
        'expiration_timestamp',
        'created',
        'email',
        'payout_url',
    ];

    public static function fromXendit($data){
        return [
            "id" => $data["id"],
            "external_id" => $data["external_id"],
            "amount" => $data["amount"],
            "merchant_name" => $data["merchant_name"],
            "status" => $data["status"],
            "expiration_timestamp" => $data["expiration_timestamp"],
            "created_at" => $data["created"],
            "email" => $data["email"],
            "payout_url" => $data["payout_url"],
        ];
    }
}
