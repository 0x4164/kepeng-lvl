<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Models\Transaction;

class XenQR extends Model
{
    protected $table="xen_qr";
    protected $fillable = [
        "id",
        "external_id",
        "amount",
        "qr_string",
        "callback_url",
        "type",
        "status",
    ];

    public static function fromXendit($data){
        return [
            "id" => $data["id"],
            "external_id" => $data["external_id"],
            "amount" => $data["amount"],
            "qr_string" => $data["qr_string"],
            "callback_url" => $data["callback_url"],
            "type" => $data["type"],
            "status" => $data["status"],
            "created_at" => $data["created"],
            "updated_at" => $data["updated"],
        ];
    }
}
