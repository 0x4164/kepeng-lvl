<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Models\Transaction;

class Topup extends Model
{
    protected $table="top_up_history";

    protected $fillable = [
        "verifikator",
        "status"
    ];

    public function invoice(){
        return $this->hasOne(Invoices::class, 'id_transaction', 'kode_unik');
    }
}
