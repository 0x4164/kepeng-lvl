<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Models\Transaction;

class Invoices extends Model
{
    protected $table="invoices";
    protected $fillable = [
        'id',
        'invoice_url',
        'id_transaction',
        'external_id',
        'user_id',
        'is_high',
        'payment_method',
        'status',
        'merchant_name',
        'amount',
        'paid_amount',
        'bank_code',
        'paid_at',
        'payer_email',
        'description',
        'adjusted_received_amount',
        'fees_paid_amount',
        'expiry_date',
        'updated',
        'created',
        'currency',
        'payment_channel',
        'payment_destination',
    ];

    public function topup(){
        return $this->belongsTo(Topup::class, 'id_transaction', 'kode_unik');
    }
}
