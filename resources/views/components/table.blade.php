<div class="row">
    <div class="col-12 col-md-12 col-lg-12">
        <div class="card">
            {{-- <div class="card-header">
                <h4>Slider Mobile</h4>
            </div> --}}
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-borderer table-striped" style="width:100%" id="{{ $id }}">
                        <thead>
                            <tr>
                                @foreach ( $th as $data )
                                <th>{{ $data }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>