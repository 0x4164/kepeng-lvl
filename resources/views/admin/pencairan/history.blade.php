@extends('admin.partials.default')
@section('content')
<section class="section">

    <x-header>History Pencairan User</x-header>
    {{-- <x-button id="btnCreate" style="margin-bottom: 10px" title="Add User" href="{{ route('post.create') }}"> --}}
    {{-- </x-button> --}}

    @table([ 'th' => ['no', 'kode', 'User','No Hp','Pencairan','Biaya Admin','Total Terima','Verifikator','Status','Tanggal','verifikasi'],
    'id' => 'table_pencairan'])

</section>


<form id="deleteForm" method="POST" role="form">
    @csrf
    @method('post')
    <x-modal id="deleteModal" idForm="deleteForm" title="">



        <x-slot name="footer">
            <div class="form-group">

                <input hidden type="text" class="form-control" id="no_hp2" name="no_hp" value="">
                <input hidden type="text" class="form-control" id="kepeng2" name="kepeng" value="">
                <input hidden type="text" class="form-control" id="kode2" name="kode" value="">
            </div>


            <button type="submit" class="btn btn-danger" title="Batal" data-dismiss="modal">Batal</button>
            <button type=" submit" class="btn btn-info" onclick="formSubmit()">Verifikasi</button>
        </x-slot>
    </x-modal>
</form>


<form id="rejectForm" method="POST" role="form">
    @csrf
    @method('post')
    <x-modal id="rejectModal" idForm="rejectForm" title="">



        <x-slot name="footer">

            <div class="form-group">
                <input hidden type="text" class="form-control" id="no_hp" name="no_hp" value="">
                <input hidden type="text" class="form-control" id="kepeng" name="kepeng" value="">
                <input hidden type="text" class="form-control" id="kode" name="kode" value="">
            </div>


            <button type="submit" class="btn btn-danger" title="Batal" data-dismiss="modal">Batal</button>
            <button type=" submit" class="btn btn-info" onclick="formSubmit()">Reject</button>
        </x-slot>
    </x-modal>
</form>


<script type="text/javascript">
    $(function () {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
        });
  
        table = $('#table_pencairan').DataTable({
                  processing:true,
                  serverSide:true,
                  pageLength: 5,
                  paging: true,
                  ajax: '{!! route('admin.pencairan.history') !!}',
                  columns: [
                        { data: 'DT_RowIndex',orderable:false, searchable:false, width:'10px'}, 
                        { data: 'kode'},
                        { data: 'user_nama'},
                        { data: 'no_hp'},
                        { data: 'pencairan',render: function(data, type, full, meta) {
                              var format = $.fn.dataTable.render.number( ',', '.', 0 ).display(data);
                              return 'Rp. '+format;
                              }},
                        { data: 'biaya_admin',render: function(data, type, full, meta) {
                              var format = $.fn.dataTable.render.number( ',', '.', 0 ).display(data);
                              return 'Rp. '+format;
                              }},
                        { data: 'total_terima',render: function(data, type, full, meta) {
                              var format = $.fn.dataTable.render.number( ',', '.', 0 ).display(data);
                              return 'Rp. '+format;
                              }},
                        { data: 'verifikator'},
                        { data: 'status'},
                        { data: 'created_at' },
                        { data: 'updated_at' }
                     ]
              }); 
      });


</script>


@endsection