@extends('admin.partials.default')
@section('content')
<section class="section">

    <x-header>Pencairan Masuk</x-header>
    {{-- <x-button id="btnCreate" style="margin-bottom: 10px" title="Add User" href="{{ route('post.create') }}"> --}}
    {{-- </x-button> --}}

    @table([ 'th' => ['no', 'kode', 'User','No Hp','Pencairan','Biaya Admin','Total Terima','Tanggal','Action'],
    'id' => 'table_pencairan'])

</section>


<form id="deleteForm" method="POST" role="form">
    @csrf
    @method('post')
    <x-modal id="deleteModal" idForm="deleteForm" title="">



        <x-slot name="footer">
            <div class="form-group">
                <input  type="text" class="form-control" id="biaya_admin2" name="biaya_admin2" value="">
                <input  type="text" class="form-control" id="pencairan2" name="pencairan2" value="">
                <input  type="text" class="form-control" id="user_id2" name="user_id2" value="">
                <input  type="text" class="form-control" id="kode2" name="kode2" value="">
            </div>


            <button type="submit" class="btn btn-danger" title="Batal" data-dismiss="modal">Batal</button>
            <button type=" submit" class="btn btn-info" onclick="formSubmit()">Verifikasi</button>
        </x-slot>
    </x-modal>
</form>


<form id="rejectForm" method="POST" role="form">
    @csrf
    @method('post')
    <x-modal id="rejectModal" idForm="rejectForm" title="">



        <x-slot name="footer">

            <div class="form-group">
                <input  type="text" class="form-control" id="biaya_admin" name="biaya_admin" value="">
                <input  type="text" class="form-control" id="pencairan" name="pencairan" value="">
                <input  type="text" class="form-control" id="user_id" name="user_id" value="">
                <input  type="text" class="form-control" id="kode" name="kode" value="">
            </div>


            <button type="submit" class="btn btn-danger" title="Batal" data-dismiss="modal">Batal</button>
            <button type=" submit" class="btn btn-info" onclick="formSubmit()">Reject</button>
        </x-slot>
    </x-modal>
</form>


<script type="text/javascript">
    $(function () {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
        });
  
        table = $('#table_pencairan').DataTable({
                  processing:true,
                  serverSide:true,
                  pageLength: 5,
                  paging: true,
                  ajax: '{!! route('admin.pencairan.index') !!}',
                  columns: [
                        { data: 'DT_RowIndex',orderable:false, searchable:false, width:'10px'}, 
                        { data: 'kode'},
                        { data: 'user_nama'},
                        { data: 'no_hp'},
                        { data: 'pencairan',render: function(data, type, full, meta) {
                              var format = $.fn.dataTable.render.number( ',', '.', 0 ).display(data);
                              return 'Rp. '+format;
                              }},
                        { data: 'biaya_admin',render: function(data, type, full, meta) {
                              var format = $.fn.dataTable.render.number( ',', '.', 0 ).display(data);
                              return 'Rp. '+format;
                              }},
                        { data: 'total_terima',render: function(data, type, full, meta) {
                              var format = $.fn.dataTable.render.number( ',', '.', 0 ).display(data);
                              return 'Rp. '+format;
                              }},
                     
                        { data: 'created_at' },
                        { data: 'action',width:'60px' }
                     ]
              }); 


              $('#table_pencairan tbody').on('click', '.delete', function () {
                        $('#deleteModal').modal('show');  
                        let data=table.row($(this).parents('tr') ).data();
                        var url = 'pencairan/verifikasi/' + data.kode;
                        $("#deleteForm").attr('action', url); 
                        document.getElementById("biaya_admin2").value = data.biaya_admin; 
                        document.getElementById("pencairan2").value = data.pencairan; 
                        document.getElementById("kode2").value = data.kode; 
                        document.getElementById("user_id2").value = data.user_id; 
                        $('#deleteModal .modal-title').text("Apakah anda ingin memverifikasi pencairan dengan kode " +data.kode+" ?");
               }); 

             function formSubmit() { 
                    $("#deleteForm").submit(); 
            }

            $('#table_pencairan tbody').on('click', '.reject', function () {
                    $('#rejectModal').modal('show');  
                    let data=table.row($(this).parents('tr') ).data();
                    var url = 'pencairan/reject/' + data.kode;
                    $("#rejectForm").attr('action', url); 
                    document.getElementById("biaya_admin").value = data.biaya_admin; 
                    document.getElementById("pencairan").value = data.pencairan; 
                    document.getElementById("user_id").value = data.user_id; 
                    document.getElementById("kode").value = data.kode; 
                    $('#rejectModal .modal-title').text("Apakah anda ingin membatalkan top up dengan kode " +data.kode+" ?"); 
                    }); 
             function formSubmit() { 
                    $("#rejectForm").submit(); 
            } 








      });


</script>


@endsection