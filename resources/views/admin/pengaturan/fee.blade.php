@extends('admin.partials.default')
@section('content')
<section class="section">
    <x-header>Transaction Fee</x-header>
    {{-- <h2>belum ada data</h2> --}}

    <div class="card">
        <div class="card-body">
            <form action="{{ url('admin/pengaturan/fee/update') }}" enctype="multipart/form-data" method="POST">
                @csrf
                @if ($errors->any())
                <div style="padding:20px; margin-bottom: 20px" class="aler alert-danger">
                    @foreach ($errors->all() as $err )
                    <li><span>{{ $err }}</span></li>
                    @endforeach
                </div>
                @endif
                <label>Transaction Fee</label>
                <div class="form-group">
                    <input type="text" class="form-control " id="fee" name="fee" placeholder=""
                        value="{{ $fee->transaction_fee ?? null}}">
                    <div class="invalid-feedback">
                    </div>
                </div>
                <button type=" submit" class="btn btn-primary">Update transaction fee</button>
            </form>
        </div>

    </div>


</section>



@endsection