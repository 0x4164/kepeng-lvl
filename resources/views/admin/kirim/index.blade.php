@extends('admin.partials.default')
@section('content')
<section class="section">

    <x-header>Transfer History User</x-header>
    {{-- <x-button id="btnCreate" style="margin-bottom: 10px" title="Add User" href="{{ route('post.create') }}"> --}}
    {{-- </x-button> --}}

    @table([ 'th' => ['no','trading_code','pengirim','penerima','Kontak Penerima','pesan','jumlah
    kepeng','Poin','Tanggal
    transfer'],
    'id' => 'tbl-kirim'])

</section>


<form id="deleteForm" method="POST" role="form">
    @csrf
    @method('post')
    <x-modal id="deleteModal" idForm="deleteForm" title="">
        <x-slot name="footer">
            <x-button color="secondary" title="Batal" data-dismiss="modal" />
            <button type="submit" class="btn btn-info" onclick="formSubmit()">Verifikasi</button>
        </x-slot>
    </x-modal>
</form>


<script type="text/javascript">
    $(function () {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
        });
  
        table = $('#tbl-kirim').DataTable({
                  processing:true,
                  serverSide:true,
             
                 
                  paging: true,
                  dom: 'Bfrtip',
                  buttons: [
            {
                extend: 'excelHtml5',
                title: 'report_transfer_user',
                exportOptions: {
            format: {
                     body: function (data, row, column, node ) {
                                return column === 4 ? "\0" + data : data;
                                }
              }
           }
            },
            {
                extend: 'csvHtml5',
                title: 'report_transfer_user'
            }
        ],
                //   url:'admin/topuphistory/masuk',
                  ajax: '{!! url('admin/kirim/history') !!}',
                  
                  columns: [
                        { data: 'DT_RowIndex',orderable:false, searchable:false, width:'10px'},
                        { data: 'trading_code'}, 
                        { data: 'pengirim'},
                        { data: 'penerima'},
                        { data: 'no_hp'},
                        { data: 'pesan'},
                        { data: 'kepeng', render: function(data, type, full, meta) {
                              var format = $.fn.dataTable.render.number( ',', '.', 0 ).display(data);
                              return 'Rp. '+format;
                              }},
                        { data: 'poin'},
                        { data: 'created_at'},
                     ]
              }); 
      });


 
</script>




@endsection