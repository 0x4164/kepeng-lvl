@extends('admin.partials.default')
@section('content')
<section class="section">

    <x-header>Admin History</x-header>
    {{-- <x-button id="btnCreate" style="margin-bottom: 10px" title="Add User" href="{{ route('post.create') }}"> --}}
    {{-- </x-button> --}}

    @table([ 'th' => ['no', 'admin','user','no_hp_user','aksi','point','kepeng','created_at'],
    'id' => 'tbl-users'])

</section>
<form id="deleteForm" method="POST" role="form">
    @csrf
    @method('delete')
    <x-modal id="deleteModal" idForm="deleteForm" title="Apakah anda yakin ingin menghapus post ?">

        <x-slot name="footer">
            <x-button color="secondary" title="Batal" data-dismiss="modal" />
            <button type="submit" class="btn btn-danger" onclick="formSubmit()">Delete Data
            </button>
        </x-slot>
    </x-modal>
</form>

<script type="text/javascript">
    $(function () {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
        });
  
        table = $('#tbl-users').DataTable({
                  processing:true,
                  serverSide:true,
                  pageLength: 5,
                  paging: true,
                  ajax: '{!! url('admin/topuphistory/admin_history') !!}',
                  columns: [
                        { data: 'DT_RowIndex',orderable:false, searchable:false, width:'10px'}, 
                        { data: 'nama_admin'},
                        { data: 'nama_user' },
                        { data: 'no_hp_user' },
                        { data: 'aksi' },
                        { data: 'poin' },
                        { data: 'kepeng', 
                        render: function(data, type, full, meta) {
                              var format = $.fn.dataTable.render.number( ',', '.', 0 ).display(data);
                              return 'Rp. '+format;
                              }},
                        { data: 'created_at' },
                   
                     ]
              }); 
      });

      $('#tbl-users tbody').on('click', '.delete', function () {
              $('#deleteModal').modal('show');  
              id = $(this).attr('id'); 
              var url = id;
              $(" #deleteForm").attr('action', url); let data=table.row( $(this).parents('tr') ).data();
              $('#deleteModal .modal-title').text("Yakin ingin menghapus Post, " +data.name+" ?"); }); 
             function formSubmit() { 
                    $("#deleteForm").submit(); 
                } 
</script>

@endsection