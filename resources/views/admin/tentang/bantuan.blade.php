@extends('admin.partials.default')
@section('content')
<section class="section">

    <x-header>Pusat Bantuan Kepeng</x-header>

    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">


    <div class="card-body">
        <form method="post" action="{{ url('admin/tentang/bantuan/simpan') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                @foreach ($content as $item)
                <textarea class="ckeditor form-control" name="content">{{ $item->content }}</textarea>
                @endforeach

            </div>
            <a href="{{ url()->previous() }}"> <button type="button" class="btn btn-secondary">Kembali</button></a>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>

    </div>

    </body>

    <script src="//cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
                $('.ckeditor').ckeditor();
            });
            var data =  $('.ckeditor').ckeditor().getData();
            console.log(data);
    </script>

    </html>
</section>





@endsection