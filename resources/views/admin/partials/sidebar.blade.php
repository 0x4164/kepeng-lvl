<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">Kepeng Admin</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">KEPENG</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class=" nav-item dropdown">
            <li class="">
                <a href="{{ url('admin/home') }}" class="nav-link"><i class=" fas
            fa-home"></i><span>Dashboard</span></a>
            </li>
            </li>
            <li class="menu-header">Menu</li>
            <li class="">


                @if(Auth::user()->hak_akses_id=='1')
            <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fa fa-history"></i>
                    <span>Top Up</span></a>


                <ul class="dropdown-menu">
                    <li><a class="nav-link active" href="{{ url('admin/topuphistory/masuk') }}"><i class=" far
                            fa-sticky-note"></i><span>Topup Masuk</span></a></li>
                    <li> <a class="nav-link active" href="{{ url('admin/topuphistory/manual') }}"><i class=" far
                                fa-sticky-note"></i><span>Topup Kepeng</span></a></li>
                    <li> <a class="nav-link active" href="{{ url('admin/topuphistory/point') }}"><i class=" far
                                    fa-sticky-note"></i><span>Topup Point</span></a></li>

                </ul>
            </li>


            <li class="nav-item">
                <a href="{{ route('admin.pencairan.index') }}" class="nav-link"><i class="fa fa-history"></i>
                    <span>Pencairan</span></a>
            </li>
            @endif


            </li>




            <li class="nav-item dropdown ">
                <a href="#" class="nav-link has-dropdown"><i class="fa fa-history"></i> <span>History</span></a>
                <ul class="dropdown-menu">
                    <li><a class=" nav-link" href="{{ route('admin.topuphistory.index') }}">
                            Top Up User</a>
                    </li>
                    <li><a class=" nav-link" href="{{ url('admin/kirim/history') }}">
                            Transfer User</a>
                    </li>
                    <li><a class=" nav-link" href="{{ url('admin/topuphistory/admin_history') }}">
                            Admin History</a>
                    </li>
                    <li><a class=" nav-link" href="{{ url('admin/pencairan/history') }}">
                        Pencairan History</a>
                </li>

                </ul>
            </li>


            <li class="nav-item dropdown ">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-user-friends"></i>
                    <span>User</span></a>


                <ul class="dropdown-menu">
                    <li><a class=" nav-link" href="{{ url('admin/user/pengguna') }}">
                            Pengguna</a>
                    </li>

                    @if(Auth::user()->hak_akses_id=='1')
                    <li><a class=" nav-link" href="{{ url('admin/user/admin') }}">
                            Admin</a>
                    </li>
                    @endif


                </ul>
            </li>

            <li class="nav-item dropdown ">
                @if(Auth::user()->hak_akses_id=='1')
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-cog"></i> <span>Pengaturan</span></a>
                @endif
                <ul class="dropdown-menu">
                    <li><a class=" nav-link" href="{{ url('admin/pengaturan/fee') }}">Transaction Fee</a>
                    </li>
                    <li><a class=" nav-link" href="{{ url('admin/slider') }}">Slider Promo
                      <li><a class=" nav-link" href="{{ url('admin/slider_berita') }}">Slider Berita
                    <li><a class=" nav-link" href="{{ url('admin/tentang/') }}">Tentang App Mobile</a>
                    </li>


                </ul>
            </li>


        </ul>
    </aside>
</div>