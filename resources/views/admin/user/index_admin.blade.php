@extends('admin.partials.default')
@section('content')
<section class="section">

    <x-header>User Management</x-header>
    <x-button id="btnCreate" style="margin-bottom: 10px" title="Tambah" href="{{ url('admin/user/admin/create') }}">
    </x-button>

    @table([ 'th' => ['no', 'Name','Tipe','created_at','Action'],
    'id' => 'tbl-users'])

</section>
<form id="deleteForm" method="POST" role="form">
    @csrf
    @method('delete')
    <x-modal id="deleteModal" idForm="deleteForm" title="Apakah anda yakin ingin menghapus Akun ?">

        <x-slot name="footer">
            <x-button color="secondary" title="Batal" data-dismiss="modal" />
            <button type="submit" class="btn btn-danger" onclick="formSubmit()">Delete Data
            </button>
        </x-slot>
    </x-modal>
</form>

<script type="text/javascript">
    $(function () {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
        });
  
        table = $('#tbl-users').DataTable({
                  processing:true,
                  serverSide:true,
                  pageLength: 5,
                  paging: true,
                  url:'admin/user/admin',
                  columns: [
                        { data: 'DT_RowIndex',orderable:false, searchable:false, width:'10px'}, 
                        { data: 'username'},
                        { data: 'tipe' },
                        { data: 'created_at' },
                        { data: 'action', width : '30px' }
                     ]
              }); 
      });

      $('#tbl-users tbody').on('click', '.delete', function () {
              $('#deleteModal').modal('show');  
              id = $(this).attr('id'); 
              var url = id;
              $(" #deleteForm").attr('action', url); let data=table.row( $(this).parents('tr') ).data();
              $('#deleteModal .modal-title').text("Yakin ingin menghapus akun, " +data.name+" ?"); }); 
             function formSubmit() { 
                    $("#deleteForm").submit(); 
                } 
</script>

@endsection