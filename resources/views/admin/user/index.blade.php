@extends('admin.partials.default')
@section('content')
<section class="section">

    <x-header>User Management</x-header>
    {{-- <x-button id="btnCreate" style="margin-bottom: 10px" title="Add User" href="{{ route('post.create') }}"> --}}
    {{-- </x-button> --}}

    @table([ 'th' => ['no', 'Name','No Hp','Saldo','Point','Tipe','Status','created_at','Action'],
    'id' => 'tbl-users'])

</section>

<form id="disableForm" method="POST" role="form">
    @csrf
    @method('post')
    <x-modal id="disableModal" idForm="disableForm" title="Apakah anda yakin ingin menghapus post ?">

        <x-slot name="footer">
            <input hidden type="text" class="form-control" id="status" name="status" value="disable">
            <input hidden type="text" class="form-control" id="no_hp2" name="no_hp">
            <x-button color="secondary" title="Batal" data-dismiss="modal" />
            <button type="submit" class="btn btn-danger" onclick="formSubmit()">suspend
            </button>
        </x-slot>
    </x-modal>
</form>

<form id="enableForm" method="POST" role="form">
    @csrf
    @method('post')
    <x-modal id="enableModal" idForm="enableForm" title="Apakah anda yakin ingin mengaktifkan user ?">

        <x-slot name="footer">
            <input hidden type="text" class="form-control" id="status" name="status" value="enable">
            <input hidden type="text" class="form-control" id="no_hp" name="no_hp">
            <x-button color="secondary" title="Batal" data-dismiss="modal" />
            <button type="submit" class="btn btn-success" onclick="formSubmit()">aktifkan
            </button>
        </x-slot>
    </x-modal>
</form>


<form id="resetForm" method="POST" role="form">
    @csrf
    @method('post')
    <x-modal id="resetModal" idForm="resetForm" title="Apakah anda yakin ingin mengaktifkan user ?">

        <x-slot name="footer">
            <input  type="text" class="form-control" id="password" name="password" placeholder="Masukan Password baru">
            <input  hidden type="text" class="form-control" id="no_hp3" name="no_hp3">
            <x-button color="secondary" title="Batal" data-dismiss="modal" />
            <button type="submit" class="btn btn-danger" onclick="formSubmit()">Reset
            </button>
        </x-slot>
    </x-modal>
</form>

<form id="resetFormPin" method="POST" role="form">
    @csrf
    @method('post')
    <x-modal id="resetPinModal" idForm="resetForm" title="Apakah anda yakin ingin mengganti pin user ?">

        <x-slot name="footer">
            <input  type="number" class="form-control" id="pin" name="pin" placeholder="Masukan PIN baru">
            <input  hidden type="text" class="form-control" name="nohp">
            <x-button color="secondary" title="Batal" data-dismiss="modal" />
            <button type="submit" class="btn btn-danger">Reset</button>
        </x-slot>
    </x-modal>
</form>



<script type="text/javascript">
    $(function () {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
        });
  
        table = $('#tbl-users').DataTable({
                  processing:true,
                  serverSide:true,
                  paging: true,
                //   url:'admin/user/pengguna',
                  ajax: '{!! url('admin/user/pengguna') !!}',
                  columns: [
                        { data: 'DT_RowIndex',orderable:false, searchable:false, width:'10px'}, 
                        { data: 'name'},
                        { data: 'no_hp' },
                        { data: 'saldo',
                          render: function(data, type, full, meta) {
                              var format = $.fn.dataTable.render.number( ',', '.', 0 ).display(data);
                              return 'Rp. '+format;
                              }},
                        { data: 'poin' },
                        { data: 'tipe' },
                        { data: 'active' },
                        { data: 'created_at' },
                        { data: 'action' },
                        // { data: 'action', width : '30px' }
                     ]
              }); 
      });

      $('#tbl-users tbody').on('click', '.disable', function () {
              $('#disableModal').modal('show');  
              id = $(this).attr('id'); 
              var url = 'pengguna/status/'+id;
              $(" #disableForm").attr('action', url);
               let data=table.row( $(this).parents('tr') ).data();
              document.getElementById("no_hp2").value = data.no_hp; 
              $('#disableModal .modal-title').text("Apakah anda Yakin ingin mensuspend user : " +data.name+" ?"); }); 
             function formSubmit() { 
                    $("#disableForm").submit(); 
                } 

                $('#tbl-users tbody').on('click', '.enable', function () {
              $('#enableModal').modal('show');  
              id = $(this).attr('id'); 
              var url = 'pengguna/status/'+id;
              $(" #enableForm").attr('action', url); 
              let data=table.row( $(this).parents('tr') ).data();
              document.getElementById("no_hp").value = data.no_hp; 
              $('#enableModal .modal-title').text("Apakah anda Yakin ingin mengaktifkan user : " +data.name+" ?"); }); 
             function formSubmit() { 
                    $("#enableForm").submit(); 
                } 
                
           $('#tbl-users tbody').on('click', '.reset', function () {
              $('#resetModal').modal('show');  
              id = $(this).attr('id'); 
              var url = 'pengguna/resetpass/'+id;
              $(" #resetForm").attr('action', url); 
              let data=table.row( $(this).parents('tr') ).data();
              document.getElementById("no_hp3").value = data.no_hp; 
              $('#resetModal .modal-title').text("Apakah anda Yakin ingin Mereset Password user : " +data.name+" ?"); }); 
             function formSubmit() { 
                    $("#resetForm").submit(); 
                } 

                $('#tbl-users tbody').on('click', '.resetpin', function () {
              $('#resetPinModal').modal('show');  
              id = $(this).attr('id'); 
              var url = 'pengguna/resetpin/'+id;
              $(" #resetFormPin").attr('action', url); 
              let data=table.row( $(this).parents('tr') ).data();
              $('#resetFormPin').find('input[name=nohp]').val(data.no_hp); 
              $('#resetPinModal .modal-title').text("Apakah anda Yakin ingin Mereset PIN user : " +data.name+" ?"); }); 
</script>

@endsection