@extends('admin.partials.default')
@section('content')
<section class="section">

    <x-header>Top Up Masuk</x-header>
    {{-- <x-button id="btnCreate" style="margin-bottom: 10px" title="Add User" href="{{ route('post.create') }}"> --}}
    {{-- </x-button> --}}

    @table([ 'th' => ['no', 'kode Topup', 'User','No Hp','Jumlah Topup','Trans Fee','Total','Verifikasi','Tanggal
    Topup','Action'],
    'id' => 'tbl-topup'])

</section>


<form id="deleteForm" method="POST" role="form">
    @csrf
    @method('post')
    <x-modal id="deleteModal" idForm="deleteForm" title="">



        <x-slot name="footer">
            <div class="form-group">

                <input hidden type="text" class="form-control" id="no_hp2" name="no_hp" value="">
                <input hidden type="text" class="form-control" id="kepeng2" name="kepeng" value="">
                <input hidden type="text" class="form-control" id="kode2" name="kode" value="">
            </div>


            <button type="submit" class="btn btn-danger" title="Batal" data-dismiss="modal">Batal</button>
            <button type=" submit" class="btn btn-info" onclick="formSubmit()">Verifikasi</button>
        </x-slot>
    </x-modal>
</form>


<form id="rejectForm" method="POST" role="form">
    @csrf
    @method('post')
    <x-modal id="rejectModal" idForm="rejectForm" title="">



        <x-slot name="footer">

            <div class="form-group">
                <input hidden type="text" class="form-control" id="no_hp" name="no_hp" value="">
                <input hidden type="text" class="form-control" id="kepeng" name="kepeng" value="">
                <input hidden type="text" class="form-control" id="kode" name="kode" value="">
            </div>


            <button type="submit" class="btn btn-danger" title="Batal" data-dismiss="modal">Batal</button>
            <button type=" submit" class="btn btn-info" onclick="formSubmit()">Reject</button>
        </x-slot>
    </x-modal>
</form>


<script type="text/javascript">
    $(function () {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
        });
  
        table = $('#tbl-topup').DataTable({
                  processing:true,
                  serverSide:true,
                  pageLength: 5,
                  paging: true,
                //   url:'admin/topuphistory/masuk',
                  ajax: '{!! url('admin/topuphistory/masuk') !!}',
                  columns: [
                        { data: 'DT_RowIndex',orderable:false, searchable:false, width:'10px'}, 
                        { data: 'kode_unik'},
                        { data: 'nama'},
                        { data: 'no_hp'},
                        { data: 'kepeng', 
                        render: function(data, type, full, meta) {
                              var format = $.fn.dataTable.render.number( ',', '.', 0 ).display(data);
                              return 'Rp. '+format;
                              }},
                              { data: 'transaction_fee',  render: function(data, type, full, meta) {
                              var format = $.fn.dataTable.render.number( ',', '.', 0 ).display(data);
                              
                              return 'Rp. '+format;
                              }},
                              { data: 'total',  render: function(data, type, full, meta) {
                              var format = $.fn.dataTable.render.number( ',', '.', 0 ).display(data);
                              return 'Rp. '+format;
                              }},
                        { data: 'status'},
                        { data: 'created_at' },
                        { data: 'action',width:'60px' }
                     ]
              }); 
      });


    $('#tbl-topup tbody').on('click', '.delete', function () {
              $('#deleteModal').modal('show');  
             let data=table.row($(this).parents('tr') ).data();
              var url = 'masuk/verifikasi/' + data.kode_unik;
              $("#deleteForm").attr('action', url); 
              document.getElementById("no_hp").value = data.no_hp; 
              document.getElementById("kepeng").value = data.kepeng; 
              document.getElementById("kode").value = data.kode_unik; 
              $('#deleteModal .modal-title').text("Apakah anda ingin memverifikasi top up dengan kode " +data.kode_unik+" ?"); }); 
             function formSubmit() { 
                    $("#deleteForm").submit(); 
            } 

            $('#tbl-topup tbody').on('click', '.reject', function () {
              $('#rejectModal').modal('show');  
             let data=table.row($(this).parents('tr') ).data();
              var url = 'masuk/reject/' + data.kode_unik;
              $("#rejectForm").attr('action', url); 
              document.getElementById("no_hp2").value = data.no_hp; 
              document.getElementById("kepeng2").value = data.kepeng; 
              document.getElementById("kode2").value = data.kode_unik; 
              $('#rejectModal .modal-title').text("Apakah anda ingin membatalkan top up dengan kode " +data.kode_unik+" ?"); }); 
             function formSubmit() { 
                    $("#rejectForm").submit(); 
            } 
</script>


@endsection