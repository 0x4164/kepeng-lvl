@extends('admin.partials.default')
@section('content')

<section class="section">

    <x-header>Tambah Point User</x-header>
    {{-- <h2>belum ada data</h2> --}}
    @include('sweetalert::alert')

    <div class="card">
        <div class="card-body">




            <form id="poinform" action="{{ url('admin/topuphistory/point/add') }}" enctype="multipart/form-data"
                method="POST">
                @csrf
                @method('post')
                @if ($errors->any())
                <div style="padding:20px; margin-bottom: 20px" class="aler alert-danger">
                    @foreach ($errors->all() as $err )
                    <li><span>{{ $err }}</span></li>
                    @endforeach
                </div>
                @endif


                <label>Pilih User</label>
                <div class="form-group">
                    <select id="no_hp" name="no_hp" class="form-control select2" style="width:100%!important;">
                        {{-- <option value="" holder></option> --}}
                        @foreach ($users as $item)
                        <option value="{{ $item->no_hp}},{{ $item->id}},{{ $item->name}},{{ $item->poin}}">
                            {{ $item->name }} ( {{ $item->no_hp }}) </option>
                        @endforeach
                    </select>
                </div>

                <label>Point sekarang</label>
                <div class="form-group">
                    <input type="text" readonly="readonly" class="form-control" id="user_point" name="user_point"
                        rows="10">

                </div>


                <label>Pilih aksi</label>
                <div class="form-group">
                    <select id="aksi" name="aksi" class="form-control select2" style="width:100%!important;">
                        {{-- <option value="" holder></option> --}}

                        <option value="penambahan">Tambah</option>
                        <option value="pengurangan">Kurangi</option>

                    </select>
                </div>



                <div class="form-group">
                    <input type="text" class="form-control" id="user" name="user" rows="10">
                </div>
                <x-input id="point" name="point" label="Masukan Jumlah Point" placeholder="0" />


                <button type="button" data-toggle="modal" data-target="#myModal" id="btn_confirm"
                    class="btn btn-primary">Proses Sekarang</button>

            </form>
        </div>


    </div>

</section>
<x-modal id="deleteModal" idForm="deleteForm" title="">
    <x-slot name="footer">
        <x-button color="secondary" title="Batal" data-dismiss="modal" />
        <button type="submit" class="btn btn-info" onclick="formSubmit()">Verifikasi</button>
    </x-slot>
</x-modal>

<script>
    $('#deleteModal').on('show.bs.modal', function () {

$('.modal-title').text('My New Modal Title');

});



    $('#btn_confirm').on('click',  function () {
              $('#deleteModal').modal('show');  
          
              $("#deleteForm").attr('action', url); 
              $("#deleteModal #myModalLabel").text( title );
              $('#deleteModal title').text("Apakah anda ingin memverifikasi top up dengan kode " +data.kode_unik+" ?"); 
               }); 

               
             function formSubmit() { 
                    $("#poinform").submit(); 
            }
            



    document.getElementById("user").style.display = "none";
    data();
    $(function(){
  
    $('.select2').on('change', function() {
        
   data();
    
    });
  });

function data(){
    var data = $(".select2 option:selected").text();
      document.getElementById("user").value = $("#no_hp").val(); 
   
      var str = $("#no_hp").val();
      var n = str.lastIndexOf(',');
      var result = str.substring(n + 1);
      document.getElementById("user_point").value = result ; 
}
  

</script>

@endsection