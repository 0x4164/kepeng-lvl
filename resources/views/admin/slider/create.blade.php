@extends('admin.partials.default')
@section('content')
<section class="section">
    <x-header>Tambah Slider</x-header>
    {{-- <h2>belum ada data</h2> --}}

    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.slider.store') }}" enctype="multipart/form-data" method="POST">
                @csrf
                @if ($errors->any())
                <div style="padding:20px; margin-bottom: 20px" class="aler alert-danger">
                    @foreach ($errors->all() as $err )
                    <li><span>{{ $err }}</span></li>
                    @endforeach
                </div>
                @endif
                <x-input id="title" name="title" label="Title" placeholder="Input Tittle ..." />

                <label>Content</label>
                <div class="form-group">

                    <textarea class="form-control" id="content" name="content" rows="10"
                        style="height: 120px;"></textarea>
                </div>

                <x-input type="file" id="thumbnail" name="thumbnail" label="Thumbnail" />
                <x-button color="success" title="Back" href="{{ route('admin.slider.index')}}" />
                <button type="submit" class="btn btn-primary">Tambah Slider</button>
            </form>
        </div>

    </div>


</section>



@endsection